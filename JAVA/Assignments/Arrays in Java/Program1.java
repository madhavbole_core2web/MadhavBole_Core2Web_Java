import java.io.*;
class Program1{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){
			int num = i;
			for(int j = 1; j <= row+1-i; j++){
				System.out.print(num++ + " ");
			}
			System.out.println();
		}
	}
}

