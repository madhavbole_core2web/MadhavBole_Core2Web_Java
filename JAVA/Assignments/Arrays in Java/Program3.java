import java.io.*;
class Program3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int row = Integer.parseInt(br.readLine());
		int num = row * (row+1);

		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row+1-i; j++){
				System.out.print(num + " ");
				num -= 2;
			}
			System.out.println();
		}
	}
}

