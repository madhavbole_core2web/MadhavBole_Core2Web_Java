import java.util.*;
class Program4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int row = sc.nextInt();
		int num = 0;
		
		for(int i = row; i >= 1; i--){
			num = num + i;
		}

		char ch = (char)(num+64);
		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row+1-i; j++){
				System.out.print(ch-- + " ");
			}
			System.out.println();
		}
	}
}

