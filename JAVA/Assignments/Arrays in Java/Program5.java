import java.io.*;
class Program5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){
			if(i%2!=0){
				char ch = 'A';
				for(int j = 1; j <= row+1-i; j++){
                                	System.out.print(ch++ + " ");
                        	}
			}else{
				char ch = 'a';
				for(int j = 1; j <= row+1-i; j++){
                                	System.out.print(ch++ + " ");
                        	}
			}
			System.out.println();
		}
	}
}

