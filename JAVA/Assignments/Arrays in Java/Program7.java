import java.util.Scanner;
class Program7{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int row = sc.nextInt();
		
		for(int i = 1; i <= row; i++){
			int num = row+1-i;
			for(int j = 1; j <= row+1-i; j++){
				if(j%2!=0){
					System.out.print(num + " ");
				}else{
					System.out.print((char)(num+64) + " ");
				}
				num--;
			}
			System.out.println();
		}
	}
}
