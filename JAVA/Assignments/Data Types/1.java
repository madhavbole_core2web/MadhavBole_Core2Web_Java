// There are 12 divisions of 11th class. So, byte is the best data type to use for this scenario.

class Assignment{
	public static void main(String[] args){
		byte standard = 11;
		byte no_of_divisions = 12;

		System.out.println("There are " + no_of_divisions + " divisions of standard " + standard);
	}
}
