// A movie can earn more than 214 crores. So we can use long data type for this.
// imdb rating of a movie is a decimal value. So we can use float data type for representign imdb rating.

class Assignment10{
        public static void main(String[] args){
		long earning = 65498732165484l;
		float imdb = 9.2f;
                System.out.println(earning);
		System.out.println(imdb);
        }
}
