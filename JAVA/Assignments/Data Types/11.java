// Gravity of a earth is 9.8 i.e., float value so float data type.
// The letter 'g' is used to represent the earth gravity. So char data type we will use.

class Assignment11{
        public static void main(String[] args){
		float gravity1 = 9.8f;
		char gravity2 = 'g';
                System.out.println(gravity1);
		System.out.println(gravity2);
        }
}
