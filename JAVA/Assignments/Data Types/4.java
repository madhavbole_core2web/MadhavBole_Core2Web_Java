/*
 Range of int data type is : -2147483648 to 2147483647. We can use "int" datatype for the employee count in company. And we can use "long" datatype for expenses to travel*/

class Assignment4{
        public static void main(String[] args){
		int employee = 654987;
		long expense = 32165498765431654l;
                System.out.println("Number of Employee = " + employee);
		System.out.println("Total travel expenses = " + expense);
        }
}
