// The number of buttons on keyboard cannot exceed 127. So we can use byte data type for this putpose.
// The cost of keyboard can be denoted using int data type.

class Assignment5{
        public static void main(String[] args){

		byte buttons = 104;
		int cost = 3216548;
                System.out.println("No. of buttons = " + buttons);
		System.out.println("Cost of keyboard = " + cost + " Rs");
        }
}
