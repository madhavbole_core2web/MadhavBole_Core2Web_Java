class Assignment6{
        public static void main(String[] args){
		byte date = 29;				// It can never exceed 127.
		byte month = 01;			// It can never exceed 127.
		short year = 2024;			// It can never exceed range of byte.
		int DaySeconds = 86000;			// It can never exceed range of int.
		int  MonthSeconds = 259200;		// It can never exceed range of int.
		long YearSeconds = 31104000l;		// It can never exceed range of long.

		System.out.println(date);
		System.out.println(month);
		System.out.println(year);
		System.out.println(DaySeconds);
		System.out.println(MonthSeconds);
		System.out.println(YearSeconds);
	}
}
