import java.io.*;
class Code2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter age: ");
		int age = Integer.parseInt(br.readLine());
		if(age <= 0){
			System.out.println("Please enter valid age");
		}else{
			if(age>18){
				System.out.println("Voter is eligible for voting");
			}else{
				System.out.println("Voter is not eligible for voting");
			}
		}
	}
}
