import java.io.*;
class Code8{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter first number: ");
		int num1 = Integer.parseInt(br.readLine());
		System.out.println("Enter second number: ");
		int num2 = Integer.parseInt(br.readLine());
		
		int sum = 0;
		for(int i = num1; i <= num2; i++){
			sum = sum + i;
		}

		System.out.println(sum);
	}
}

