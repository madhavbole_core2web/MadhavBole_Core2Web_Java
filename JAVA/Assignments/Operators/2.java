class Relational{
	public static void main(String[] args){

		int x = 15;
		int y = 39;

		System.out.println("15 > 39 : " + (x>y));
		System.out.println("15 < 39 : " + (x<y));
		System.out.println("15 == 39 : " + (x==y));
		System.out.println("15 != 39 : " + (x!=y));
		System.out.println("15 >= 39 : " + (x>=y));
		System.out.println("15 <= 39 : " + (x<=y));

	}
}
