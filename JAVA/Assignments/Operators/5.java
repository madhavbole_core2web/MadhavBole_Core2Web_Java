// Bitwise Operators : & , | , ^ , ~ , << , >> , >>> .
//
class Bitwise{
	public static void main(String[] args){

		int x = 10;	// 0000 1010
		int y = 15;	// 0000 1111

		System.out.println("Logical AND : 10 & 15 : " + (10&15));
		System.out.println("Logical OR : 10 | 15 : " + (10|15));
		System.out.println("Negation : ~10 : " + (~10));
		System.out.println("Bitwise XOR : 10 ^ 15 : " + (x^y));
		System.out.println("Left Shift : 10 << 2 : " + (x<<2));
		System.out.println("Right Shift : 10 >> 3 : " + (x>>3));

	}
}


