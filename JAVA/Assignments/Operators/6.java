class Assignment{
	public static void main(String[] args){

		int a = 10;
		int b = 15;

		a += 5;
		System.out.println("a += 5 : " + a);

		b -= 3;
		System.out.println("b -= 3 : " + b);

		a *= 2;
		System.out.println("a *= 2 : " + a);

		b /= 5;
		System.out.println("b /= 5 : " + b);

		a %= 3;
		System.out.println("a %= 3 : " + a);

	}
}

