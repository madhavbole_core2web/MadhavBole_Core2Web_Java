// Write a program to check whether the given number is odd or even

class Program1A{
	public static void main(String[] args){
		int num = 45;

		if(num % 2 != 0){
			System.out.println("Number is odd");
		}
		else{
			System.out.println("Number is even");
		}
	}
}

