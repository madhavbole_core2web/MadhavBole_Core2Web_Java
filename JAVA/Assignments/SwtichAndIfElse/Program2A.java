// Write a program to print Remarks according to their respective grades

class Program2A{
	public static void main(String[] args){
		char grade = 'O';
		if(grade == 'O'){
			System.out.println("Outstanding");
		}else if(grade == 'A'){
			System.out.println("Excellent");
		}else if(grade == 'B'){
			System.out.println("Good");
		}else if(grade == 'C'){
			System.out.println("Satisfactory");
		}else if(grade == 'D'){
			System.out.println("Less than satisfactory");
		}else{
			System.out.println("Fail");
		}
	}
}
