class Program2B{
	public static void main(String[] args){
		char grade = 'A';
		switch(grade){
			case 'O':
				System.out.println("Outstanding");
				break;

			case 'A':
				System.out.println("Excellent");
				break;

			case 'B':
				System.out.println("Good");
				break;

			case 'C':
				System.out.println("Satisfactory");
				break;

			case 'D':
				System.out.println("Less than satisfactory");
				break;

			default:
				System.out.println("Fail");
		}
	}
}
