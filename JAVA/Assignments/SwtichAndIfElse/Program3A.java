// Write a program to print longform of cloth sizes according to their acronym

class Program3A{
	static String size = "XL";
	public static void main(String[] args){
		if(size=="S"){
			System.out.println("Small");
		}else if(size == "M"){
			System.out.println("Medium");
		}else if(size == "L"){
			System.out.println("Large");
		}else if(size=="XL"){
			System.out.println("Extra Large");
		}else{
			System.out.println("Unknown size");
		}
	}
}

