// Write a program that takes number from 0 to 5 and print its spelling, if the entered number is greater than 5 print the number is greater than 5.

class Program6A{
	public static void main(String[] args){
		int number = 4;

		if(number==0){
			System.out.println("Zero");
		}
		else if(number==1){
			System.out.println("One");
		}
		else if(number==2){
			System.out.println("Two");
		}
		else if(number==3){
			System.out.println("Three");
		}
		else if(number==4){
			System.out.println("Four");
		}
		else if(number==5){
			System.out.println("Five");
		}
		else if(number>5){
			System.out.println("Number is greater than 5");
		}else{
			System.out.println("Number is negative");
		}
	}
}
