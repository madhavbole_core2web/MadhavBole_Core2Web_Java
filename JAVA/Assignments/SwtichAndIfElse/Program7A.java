/* 
Write a program for the below example:
	If you are a trip planner and you are planning a trip according to your budget of client.:
		1. For budget 15000 destination is Jammu and Kashmir.
		2. For budget 10000 destination is Manali.
		3. For budget 6000 destination is Amritsar.
		4. For budget 2000 destination is Mahabaleshwar.
		5. For Other budget try next time.
*/

class Program7A{
	static int budget = 16000;
	public static void main(String[] args){
		if(budget==15000){
			System.out.println("Destination is Jammu and Kashmir");
		}else if(budget==10000){
			System.out.println("Destination is Manali");
		}else if(budget==6000){
			System.out.println("Destination is Amritsar");
		}else if(budget==2000){
			System.out.println("Destination is Mahabaleshwar");
		}else{
			System.out.println("For other budgets try next time");
		}
	}
}

