/*
  Write a program which takes two numbers. If both the numbers are positive, multiply them and provide to switch case to varify whether the number is even   or odd, if the user enters any negative number, program should terminate by saying "Sorry negative numbers are not allowed"
*/

class Program8{
	public static void main(String[] args){
		int num1 = 25;
		int num2 = 3;
		int product = 0;
		if(num1>=0 && num2>=0){
			product = num1 * num2;
		}else{
			System.out.println("Sorry, negative numbers not allowed");
		}

		System.out.println(product);


		int evenodd = product%2;

		switch(evenodd){
			case 0:
				System.out.println("Both numbers are even.");
				break;
			case 1:
				System.out.println("One of the numbers is odd");
				break;
		}
	}
}

