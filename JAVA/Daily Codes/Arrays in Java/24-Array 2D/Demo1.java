import java.util.Scanner;

class Demo1{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for(int i = 0; i < arr.length; i++){
			System.out.print("Enter element: ");
			arr[i] = sc.nextInt();
		}

		System.out.println("Elements in array are: ");

		for(int i = 0; i < arr.length; i++){
			System.out.println(arr[i]);
		}
	}
}

