import java.util.Scanner;

class Demo2{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		int sum = 0;
		int arr[] = new int[size];

		for(int i = 0; i < arr.length; i++){
			System.out.print("Enter element: ");
			arr[i] = sc.nextInt();
			sum = sum + arr[i];
		}

		System.out.println("Elements in array are: ");

		for(int i = 0; i < arr.length; i++){
			System.out.println(arr[i]);
		}

		System.out.println("Sum of elements in array is: " + sum);
	}
}

