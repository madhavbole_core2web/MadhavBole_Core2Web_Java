class Proof{
	int x = 128;
	int y = 228;
	int z = 328;

	public static void main(String[] args){
		Proof obj = new Proof();

		System.out.println("x : " + obj.x);
		System.out.println("Address of x: " + System.identityHashCode(obj.x));
		System.out.println("y : " + obj.y);
		System.out.println("z : " + obj.z);
		System.out.println("Address of z: " + System.identityHashCode(obj.z));
	//	System.out.println("str1 : " + obj.str1);
	//	System.out.println("str2 : " + obj.str2);
	}
}
