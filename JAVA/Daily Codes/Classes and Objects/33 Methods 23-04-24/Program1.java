class Program1{

	static void fun(){
		System.out.println("In fun method");
	}

	public static void main(String[] args){
		System.out.println("In main method");

		System.out.println("Calling fun method using object of class: ");
		Program1 obj = new Program1();
		obj.fun();

		System.out.println("Calling fun method ");
		fun();

		System.out.println("Calling fun method using class name: ");
		Program1.fun();

	}
}
