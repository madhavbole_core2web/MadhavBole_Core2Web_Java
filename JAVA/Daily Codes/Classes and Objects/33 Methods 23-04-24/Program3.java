class One{

	int x = 10;
	static int y = 20;

	void fun(){
		System.out.println("In fun method");
	}

	static void run(){
		System.out.println("In run method");
	}
}

class Two{
	public static void main(String[] args){
		System.out.println("In main method");
		int z  = 10;
		One obj = new One();
		
		System.out.println("Address of x : " + System.identityHashCode(obj.x));
		System.out.println("Address of y : " + System.identityHashCode(z));
		System.out.println(obj.x);
		System.out.println(obj.y);
		obj.fun();
		obj.run();
	}
}
