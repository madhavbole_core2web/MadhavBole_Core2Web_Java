class Switch3{
	public static void main(String[] args){
		char data = 'a';

		System.out.println("Before switch");

		switch(data){
			case 'a':
				System.out.println("a");
				break;

			case 'b':
				System.out.println("b");
				break;

			case 'c':
				System.out.println("c");
				break;

			default:
				System.out.println("In default state");

		}

		System.out.println("After Switch");
	}
}

