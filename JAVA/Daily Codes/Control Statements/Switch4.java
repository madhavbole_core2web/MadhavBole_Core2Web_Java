class Switch4{
	public static void main(String[] args){

		String state = "Maharashtra";

		System.out.println("Before switch");

		switch(state){
			case "UttarPradesh":
				System.out.println("Capital: Lucknow");
				break;

			case "Uttarakhand":
				System.out.println("Capital: Dehradun");
				break;

			case "MadhyaPradesh":
				System.out.println("Capital: Bhopal");
				break;

			case "Maharashtra":
				System.out.println("Capital: Mumbai");
				break;

			default:
				System.out.println("Capital: Unknown");
				
		}

		System.out.println("After switch");
	}
}

