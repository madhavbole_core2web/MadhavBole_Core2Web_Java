class Switch5{
	public static void main(String[] s){
		float num = 2f;
		System.out.println("Before switch");

		switch(num){
			case 1:
				System.out.println("2f and 1 are equal");
				break;

			case 2:
				System.out.println("2f and 2 are equal");
				break;

			case 3:
				System.out.println("2f and 3 are equal");
				break;

			default:
				System.out.println("defualt state");
			}
		System.out.println("After switch");
	}
}
