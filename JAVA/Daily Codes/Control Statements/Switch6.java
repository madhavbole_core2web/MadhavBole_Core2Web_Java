class Switch{
	public static void main(String[] args){

		boolean data = true;
		System.out.println("Before switch");

		switch(data){
			case false:
				System.out.println("false");
				break;

			case true:
				System.out.println("true");
				break;

			default:
				System.out.println("default state");

			}
	}
}

