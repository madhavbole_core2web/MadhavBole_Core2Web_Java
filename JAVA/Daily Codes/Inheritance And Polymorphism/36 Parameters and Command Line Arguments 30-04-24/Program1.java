class XYZ{

	void gun(){
		System.out.println("In gun");
	}

	void fun(int x){
		System.out.println("In fun");
		System.out.println(x);
	}

}

class Program1{

	void run(float x, float y){
		System.out.println("In run");
		System.out.println(x);
		System.out.println(y);
	}

	public static void main(String[] args){
		System.out.println("In main");

		XYZ obj1 = new XYZ();
		obj1.gun();
		obj1.fun(10);

		Program1 obj2 = new Program1();
		obj2.run(10.5f, 20.5f);
	}

}
