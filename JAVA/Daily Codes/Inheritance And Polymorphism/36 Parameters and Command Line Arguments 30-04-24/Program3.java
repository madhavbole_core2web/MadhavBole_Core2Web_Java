class FloatDemo{

	void fun(float x){
		System.out.println("In fun");
	}

	public static void main(String[] args){
		FloatDemo obj = new FloatDemo();

		float a = 203654l;
		obj.fun(10.5f);
		obj.fun('A');
		obj.fun(10);
		obj.fun(50l);
//		obj.fun(20.5);
	}
}


/*
 *  Method Signature:
 *  		The method signature for above method fun is:	fun(float)  	=>	Method-Name(Parameter Data Type)
 *
 *  		This is used by the compiler to distinguish between the many methods having same name
 */



