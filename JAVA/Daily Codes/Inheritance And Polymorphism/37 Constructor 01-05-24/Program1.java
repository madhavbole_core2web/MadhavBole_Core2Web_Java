class Demo{
	int x = 10;
	int y = 20;

	Demo(int a , int b){
		System.out.println("In Constructor");
		x = a;
		y = b;
	}

	void printData(){
		System.out.println("x : " + x);
		System.out.println("y : " + y);
	}
}

class Program1{
	public static void main(String[] args){
		Demo obj = new Demo(40, 50);
		obj.printData();
	}
}
