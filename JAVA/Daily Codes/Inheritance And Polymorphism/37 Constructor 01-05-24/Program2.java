class Demo{
	int x = 10;
	int y = 20;

	Demo(int x, int y){
		System.out.println("In Constructor");
		System.out.println("x : " + x);
		System.out.println("y : " + y);
		x = x;
		System.out.println("x = x  : " + x);
		y = y;
		System.out.println("y = y  : " + y);
	}

	void printData(){
		System.out.println("x : " + x);
		System.out.println("y : " + y);
	}
}

class Program2{
	public static void main(String[] args){
		Demo obj = new Demo(30, 40);
		obj.printData();
	}
}
