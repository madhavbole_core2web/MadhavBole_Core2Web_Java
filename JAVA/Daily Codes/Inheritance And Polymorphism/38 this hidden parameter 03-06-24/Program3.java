class Program3{

	int x = 10;

	Program3(int x){
		System.out.println("In constructor");
		System.out.println("x : " + x);
		System.out.println("this.x : " + this.x);
	}

	void fun(){
		System.out.println(x);
	}

	public static void main(String[] args){
		System.out.println("Constructor execution for obj1 : ");
		Program3 obj1 = new Program3(20);

		System.out.println();

		System.out.println("Constructor execution for obj2 : ");
		Program3 obj2 = new Program3(30);

		System.out.println();

		System.out.println("x for obj1 : " + obj1.x);
		System.out.println("x for obj2 : " + obj2.x);

		System.out.println();

		System.out.println("fun execution for obj1 : ");
		obj1.fun();

		System.out.println();

		System.out.println("fun execution for obj2 : ");
		obj2.fun();

		obj1.x = 50;
		
		System.out.println();

		System.out.println("x for obj1 : " + obj1.x);
		System.out.println("x for obj2 : " + obj2.x);
	}
}

		
