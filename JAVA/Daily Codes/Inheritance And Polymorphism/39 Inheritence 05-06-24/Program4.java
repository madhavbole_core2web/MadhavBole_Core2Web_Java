class ParentProgram4{
	int x = 10;

	void fun(){
		System.out.println("In fun method of class ParentProgram4");
	}
}

class ChildProgram4 extends ParentProgram4{

}

class ClientProgram4{
	public static void main(String[] args){
		ChildProgram4 cp = new ChildProgram4();
		System.out.println(cp.x);
		cp.fun();

		System.out.println();

		for(int i = 0; i < args.length; i++){
			System.out.print(args[i] + "\t");
		}
	}
}
