class ParentProgram6{
	ParentProgram6(){
		System.out.println("Parent constructor");
	}
}

class ChildProgram6 extends ParentProgram6{
	ChildProgram6(){
		System.out.println("Child constructor");
	}
}

class ClientProgram6{
	public static void main(String[] args){
		ChildProgram6 cobj = new ChildProgram6();
		System.out.println("End main");
	}
}

