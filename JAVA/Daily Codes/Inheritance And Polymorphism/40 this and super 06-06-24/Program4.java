class Parent{

	int x = 10;

	Parent(){
		System.out.println("Parent Constructor");
		System.out.println(this);
	}

	void fun(){
		System.out.println("In fun");
	}

}

class Child extends Parent{

	int y = 20;

	Child(){
		System.out.println("Child Constructor");
		System.out.println(this);
	}

	void run(){
		System.out.println("In run");
	}

	public static void main(String[] args){
		Child obj = new Child();
		System.out.println(obj);
	//	Child(obj);

		System.out.println("x : " + obj.x);
		System.out.println("y : " + obj.y);

		obj.fun();
		obj.run();
	}

}
