// This example shows the inheritence between a college and different departments of college.

class College{
	College(){
		System.out.println("College name : NBN Sinhgad Technical Institutes Campus");
	}

	void attendence(){
		System.out.println("Attendence must be 75 percent");
	}
}

class FirstYear extends College{
	FirstYear(){
		super.attendence();
	}

	void subjects(){
		System.out.println("We have subjects like Physics, Chemisty, maths, mechanics, etc.,");
	}

	int TeacherCount = 16;
}

class IT extends College{
	IT(){
		super.attendence();
	}

	void subjects(){
		System.out.println("Subject like : Operating system, Computer Netwroks Security, etc.,");
	}

	int TeacherCount = 12;
}

class CS extends College{
	CS(){
		super.attendence();
	}

	void subjects(){
		System.out.println("Subjects like : Operating System, DBMS, etc.,");
	}

	int TeacherCount = 7;
}

class Execution{
	public static void main(String[] args){
		System.out.println("First Year: ");
		FirstYear FY = new FirstYear();
		FY.subjects();
		System.out.println("Teacher count: " + FY.TeacherCount);

		System.out.println();
		
		System.out.println("IT: " );
		IT it = new IT();
		it.subjects();
		System.out.println("Teacher count: " + it.TeacherCount);

		System.out.println();

		System.out.println("CS: ");
		CS cs = new CS();
		cs.subjects();
		System.out.println("Teacher count: " + cs.TeacherCount);
	}
}
