class Parent{
	Parent(){
		System.out.println("In parent");
	}
}

class Child extends Parent{
	Child(){
		super();
		System.out.println("In child");
	}

	public static void main(String[] args){
		Child obj = new Child();
	}
}
