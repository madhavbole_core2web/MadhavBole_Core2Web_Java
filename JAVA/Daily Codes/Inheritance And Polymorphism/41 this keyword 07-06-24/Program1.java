class Parent{
	int x = 10;

	Parent(){
		System.out.println("In parent constructor");
	}

	void fun(){
		System.out.println("In fun of parent class");
	}
}

class Child extends Parent{
	void fun(){
		
		System.out.println("In fun of child class");
	}

	int x = 10;
}

class Client{
	public static void main(String[] args){
		Child obj = new Child();
		obj.fun();
	}
}
