class Program2{
	int x = 20;

	Program2(){
		int x = 30;
		System.out.println(x);
		System.out.println(this.x);
	}

	void fun(){
		int x = 50;
		System.out.println(x);
		System.out.println(this.x);
	}

	public static void main(String[] args){
		Program2 obj = new Program2();
		System.out.println(obj.x);

		obj.fun();

	}
}
