class Program4{
	int x = 20;

	Program4(){
		this(10);
		System.out.println("In no para");
	}

	Program4(int x){
		this(10, 20);
		System.out.println("In para 1");
		
	}

	Program4(int x, int y){
		super();
		System.out.println("In para 2");
	}

	public static void main(String[] args){
		Program4 obj = new Program4();
	}
}
