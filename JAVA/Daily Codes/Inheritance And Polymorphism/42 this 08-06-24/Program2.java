class Program2{
	int x = 0;

	Program2(int x){
		this.x = x;
		System.out.println("In Constructor");
		System.out.println(x);
		System.out.println(this.x);

	}

	void fun(int x){
		//this.x = 1234;
		System.out.println(x);
		System.out.println(this.x);
	}

	public static void main(String[] args){
		Program2 obj1 = new Program2(120);
		Program2 obj2 = new Program2(150);

		obj1.fun(100);
		obj2.fun(200);

	}
}
		
