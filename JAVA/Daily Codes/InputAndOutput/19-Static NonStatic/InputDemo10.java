class InputDemo10{
	void fun(){
		System.out.println("In fun method");
	}

	void run(){
		System.out.println("In run method");
	}

	void gun(){
		System.out.println("In gun method");
	}

	public static void main(String[] args){
		System.out.println("In main method");
		InputDemo10 obj = new InputDemo10();
		obj.fun();
		obj.run();
		obj.gun();
	}
}

