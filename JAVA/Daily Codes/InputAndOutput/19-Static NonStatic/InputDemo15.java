class InputDemo15{
	int x = 10;
	static int y = 20;

	void fun(){
		System.out.println("In fun method");
	}

	static void run(){
		System.out.println("In run method");
	}

	public static void main(String[] args){
		InputDemo15 obj = new InputDemo15();
		System.out.println(obj.x);
		System.out.println(obj.y);
		obj.fun();
		obj.run();
	}
}
