class InputDemo17{
	int x = 10;
	static int y = 20;

	void fun(){
		System.out.println("In fun method");
	}

	static void run(){
		System.out.println("In run method");
		InputDemo15 obj = new InputDemo15();
		obj.fun();
	}


	public static void main(String[] args){
		InputDemo15 obj = new InputDemo15();
		run();
	}
}
