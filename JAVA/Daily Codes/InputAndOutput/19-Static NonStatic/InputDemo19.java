class InputDemo19B{
	public static void main(String[] args){
		System.out.println("In method 3");
		InputDemo19 obj2 = new InputDemo19();
		obj2.Method1();
	}
}

class InputDemo19{
	void Method1(){
		System.out.println("In method1");
		Method2();
	}

	static void Method2(){
		System.out.println("In Method2");
	}

	public static void main(String[] args){
		System.out.println("In main method");
		InputDemo19 obj = new InputDemo19();
		obj.Method1();	
	}	
}
