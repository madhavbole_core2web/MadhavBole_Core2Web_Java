import java.io.*;
class Code5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number: ");
		int num = Integer.parseInt(br.readLine());

		if(num > 160){
			System.out.println(num + " is not in the table of 16");
		}else{
			if(num % 16 == 0){
				System.out.println(num + " is in the table of 16");
			}else{
				System.out.println(num + " is not in the table of 16");
			}
		}
	}
}

