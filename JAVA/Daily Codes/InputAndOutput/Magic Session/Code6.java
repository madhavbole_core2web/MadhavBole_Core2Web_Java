import java.io.*;
class Code6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter first number: ");
		int num1 = Integer.parseInt(br.readLine());
		System.out.println("Enter second number: ");
		int num2 = Integer.parseInt(br.readLine());
	
		for(int i = num1; i <= num2; i++){
			System.out.print(i + ", ");
		}
	}
}

