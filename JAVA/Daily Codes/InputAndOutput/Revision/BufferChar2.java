import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

class BufferedChar2{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.print("Enter name: ");
		char name = (char)br.read();

		br.skip(1);

		System.out.print("Enter flat: ");
		int flat = Integer.parseInt(br.readLine());

		System.out.print("Enter floor: ");
		int floor = Integer.parseInt(br.readLine());
	}
}

