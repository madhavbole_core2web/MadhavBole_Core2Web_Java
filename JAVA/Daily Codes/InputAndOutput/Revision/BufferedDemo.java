import java.io.*;
class BufferedDemo{
	public static void main(String[] Madhav)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		// byte int long float double char boolean

		System.out.print("Enter byte value");
		byte byteInput = Byte.parseByte(br.readLine());

		System.out.print("Enter int value");
		int intInput = Integer.parseInt(br.readLine());
		
		System.out.print("Enter long value");
		long longInput = Long.parseLong(br.readLine());
		
		System.out.print("Enter float value");
		float floatInput = Float.parseFloat(br.readLine());
		
		System.out.print("Enter double value");
		double doubleInput = Double.parseDouble(br.readLine());
		
		System.out.print("Enter char value");
		char charInput = (char)br.read();
		
		br.skip(1);

		System.out.print("Enter boolean value");
		boolean boolInput = Boolean.parseBoolean(br.readLine());
		

		System.out.println(byteInput);
		System.out.println(intInput);
		System.out.println(longInput);
		System.out.println(doubleInput);
		System.out.println(charInput);
		System.out.println(boolInput);
	}
}

