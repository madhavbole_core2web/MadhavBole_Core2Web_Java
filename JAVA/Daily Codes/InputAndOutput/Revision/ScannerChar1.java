import java.util.*;
class ScannerChar{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter first letter of your name: ");
		char name = sc.next().charAt(0);
		System.out.println("Your name's first letter is: " + name);
	}
}

