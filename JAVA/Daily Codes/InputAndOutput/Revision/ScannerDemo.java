import java.util.Scanner;
class ScannerDemo{
	public static void main(String[] args){
		// bit byte int long float double char boolean 

		Scanner scc = new Scanner(System.in);

		System.out.print("Enter byte value: ");
		byte byteInput = scc.nextByte();

		System.out.print("Enter int value: ");
		int intInput = scc.nextInt();

		System.out.print("Enter long value: ");
		long longInput = scc.nextLong();
		
		System.out.print("Enter float value: ");
		float floatInput = scc.nextFloat();
		
		System.out.print("Enter double value: ");
		double doubleInput = scc.nextDouble();
		
		System.out.print("Enter char value: ");
		char charInput = scc.next().charAt(0);
		
		System.out.print("Enter boolean value: ");
		boolean boolInput = scc.nextBoolean();


		System.out.print(byteInput);
		System.out.print(intInput);
		System.out.print(longInput);
		System.out.print(floatInput);
		System.out.print(doubleInput);
		System.out.print(charInput);
		System.out.print(boolInput);
	}
}

