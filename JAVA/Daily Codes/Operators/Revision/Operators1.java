class Operators1{
	public static void main(String[] args){
		float num1 = 20.05f;
		float num2 = 11.00f;
		
		float ans = (num2 - num1);
		System.out.println("num2 - num1: " + ans);

		num1 = ans % 2;
		System.out.println("num1 = ans % 2 : " + num1);		
	}
}
