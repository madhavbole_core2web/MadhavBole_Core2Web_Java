class Code5{
	public static void main(String[] args){
		String str1 = "Madhav";
		String str2 = "Bole";
		String str3 = str1 + str2;
		String str4 = new String("MadhavBole");
		
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}

