class Code7{
	public static void main(String[] args){
		String str1 = "Madhav";
		String str2 = "Bole";
		String str3 = new String("MadhavBole");
		String str4 = str3;

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}
