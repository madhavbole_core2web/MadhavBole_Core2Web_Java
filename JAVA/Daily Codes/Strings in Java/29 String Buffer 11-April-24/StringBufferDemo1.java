class StringBufferDemo1{
	public static void main(String[] args){
		String name1 = "Madhav";
		String name2 = "Madhav";
		System.out.println(name1);
		System.out.println(System.identityHashCode(name1));

		name1 = name1 + " Bole";
		System.out.println(name1);
		System.out.println(System.identityHashCode(name1));

		System.out.println(name2);
		System.out.println(System.identityHashCode(name2));
	}
}

