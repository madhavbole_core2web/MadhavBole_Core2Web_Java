class StringBufferDemo2{
	public static void main(String[] args){

		String str = new String("Madhav");
		System.out.println(str);
		System.out.println(System.identityHashCode(str));

		str = str + " Subhash";
		System.out.println(str);
		System.out.println(System.identityHashCode(str));

		str = str + " Bole";
		System.out.println(str);
		System.out.println(System.identityHashCode(str));
	}
}
