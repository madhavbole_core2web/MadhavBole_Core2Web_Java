class StringBufferDemo3{
	public static void main(String[] args){
		StringBuffer sb = new StringBuffer("Madhav");
		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));
		System.out.println(sb.capacity());

		sb = sb.append(" Bole");
		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));
		System.out.println(sb.capacity());

		sb = sb.append(" NBN ST.I.C");
		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));

		sb = sb.append(" Pune");
		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));
		System.out.println(sb.capacity());
	}
}
