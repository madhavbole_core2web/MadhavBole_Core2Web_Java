class StringBufferDemo4{
	public static void main(String[] args){
		StringBuffer sb = new StringBuffer("Madhav");
		sb = sb.append(" Bole");

		System.out.println("Before : " + sb);
		
		System.out.println("After using the reverse method: " + sb.reverse());
	}
}
