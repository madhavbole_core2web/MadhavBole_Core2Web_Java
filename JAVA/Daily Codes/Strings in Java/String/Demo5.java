class Demo5{
	public static void main(String[] args){
		
		String name1 = "Madhav"; 						// String literal----->	String Constant Pool->	Method area
		String name2 = new String("Madhav");					//------------------------------------------->	Heap
		char name3[] = new char[]{'M', 'a', 'd', 'h', 'a', 'v'};		//------------------------------------------->	Heap
	
		System.out.println(name1);
		System.out.println(name2);
		System.out.println(name3);


		System.out.println(System.identityHashCode(name1));
		System.out.println(System.identityHashCode(name2));
		System.out.println(System.identityHashCode(name3));

	
	}
}

