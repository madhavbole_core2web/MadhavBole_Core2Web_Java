class Switch13{
	public static void main(String[] args){
		int num = 60;
		System.out.println("Before switch");

		switch(num){
			case 30+30:
				System.out.println("30+30");
				break;
			
			case 61:
				System.out.println("61");
				break;

			case 62:
				System.out.println("62");
				break;

			default:
				System.out.println("default");
		}
		System.out.println("After switch");
	}
}

