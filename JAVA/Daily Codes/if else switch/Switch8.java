class Switch8{
        public static void main(String[] Bole){
                float num = 1.5;
                System.out.println("Before switch");

                switch(num){
                        case 1.5:
                                System.out.println("One");

                        case 2.5:
                                System.out.println("Two");

                        case 3.5:
                                System.out.println("Three");

                        default:
                                System.out.println("In default state");
                }

                System.out.println("After switch");
        }
}
