class Switch9{
        public static void main(String[] Bole){
                float num = 5f;
                System.out.println("Before switch");

                switch(num){
                        case 1.5:
                                System.out.println("1.5");

                        case 2.0:
                                System.out.println("2.0");

                        case 2.5:
                                System.out.println("2.5");

                        default:
                                System.out.println("In default state");
                }

                System.out.println("After switch");
        }
}
