class WhileDemo5{
	public static void main(String[] args){
		int day = 7;
		if(day==0){
			System.out.println("Assignment is overdue");
		}

		while(day>=1){
			System.out.println(day + " days remaining");
			day--;
		}
		
	}
}

