class WhileDemo10{
	public static void main(String[] args){
		long num = 9307922405l;
		int sum = 0;

		while(num != 0){
			long remainder = num % 10;
			sum += remainder;

			num /= 10;
		}
		System.out.println(sum);
	}
}
