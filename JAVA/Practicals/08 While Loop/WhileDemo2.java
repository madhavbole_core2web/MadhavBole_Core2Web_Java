class WhileDemo2{
	public static void main(String[] args){
		int num = 150;
		while(num <= 198){
			if(num%2!=0){
				System.out.print(num + " " );
			}

			num++;
		}
		System.out.println();
	}
}
