class WhileDemo4{
	public static void main(String[] args){
		int start = 1;
		int end = 6;

		while(start <= end){
			if(start%2==0){
				System.out.print(start + " ");
			}else{
				System.out.print((char)(start+64) + " ");
			}
			start ++;
		}
		System.out.println();
	}
}
