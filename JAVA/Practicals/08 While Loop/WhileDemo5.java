class WhileDemo5{
	public static void main(String[] args){
		int num = 1;
		int i = 1;
		while(i <= 4){
			int j = 1;
			while(j <= 4){
				System.out.print(num + " ");
				num += 2;
				j++;
			}
			i++;
			System.out.println();
		}
	}
}

