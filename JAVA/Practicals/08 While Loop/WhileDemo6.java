class WhileDemo6{
	public static void main(String[] args){
		int num = 9307;
		while(true){
			System.out.println(num%10);
			num = num / 10;
			if(num < 10){
				System.out.println(num);
				break;
			}
		}
	}
}
