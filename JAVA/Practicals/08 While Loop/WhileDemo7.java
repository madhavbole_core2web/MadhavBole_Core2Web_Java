class WhileDemo7{
	public static void main(String[] args){
		int num = 98765432;
		int counter = 0;
		while(num != 0){
			num /= 10;
			counter++;
		}
		System.out.println(counter);
	}
}
