class WhileDemo8{
	public static void main(String[] args){
		int num = 216985;
		while(num != 0){
			int remainder = num % 10;
			if(remainder % 2 != 0){
				System.out.print(remainder + " ");
			}
			num /= 10;
		}
	}
}
