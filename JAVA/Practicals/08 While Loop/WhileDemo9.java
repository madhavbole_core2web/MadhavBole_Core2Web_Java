class WhileDemo9{
	public static void main(String[] args){
		int num = 214367689;
		int OC = 0;
		int EC = 0;

		while(num != 0){
			int remainder = num % 10;
			if(remainder%2 != 0){
				OC++;
			}else{
				EC++;
			}

			num /= 10;
		}

		System.out.println("Number of Odd digits = " + OC);
		System.out.println("Number of Even digits = " + EC);

	}
}
