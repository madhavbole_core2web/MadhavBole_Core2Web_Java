import java.io.*;

class Pattern10{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int rows = Integer.parseInt(br.readLine());

		for(int i = 1; i <= rows; i++){
			int ch = 64+i;
			for(int j = i; j <= rows; j++){		// for(int j=1; j<=rows+1-i; j++)
				if(ch%2!=0){
					System.out.print(ch + " ");
				}else{
					System.out.print((char)ch + " ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}
