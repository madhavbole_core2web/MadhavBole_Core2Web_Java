import java.io.*;

class Pattern3{
	public static void main(String[] args)throws IOException{
		BufferedReader Pattern3 = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int rows = Integer.parseInt(Pattern3.readLine());

		for(int i = 1; i <= rows; i++){
			char ch = (char)(64 + i);
			for(int j = 1; j <= i; j++){
				System.out.print(ch + " ");
				ch++;
			}
			System.out.println();
		}
	}
}

