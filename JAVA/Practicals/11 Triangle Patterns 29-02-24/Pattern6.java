import java.io.*;

class Pattern6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int rows = Integer.parseInt(br.readLine());

		int num = rows;
		for(int i = 1; i <= rows ; i++){
			for(int j = rows; j >= i; j--){		// for(int j = i; j <= rows; j++)
				System.out.print(num + " ");
			}
			System.out.println();
			num--;
		}
	}
}
