import java.io.*;

class Pattern7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int rows = Integer.parseInt(br.readLine());

		for(int i = 1; i <= rows; i++){			// for(int i = 1; i <= rows; i++){
			for(int j = 1; j <= rows-i+1; j++){	// 	int num = 1;
				System.out.print(j + " ");	// 	for(int j = i; j <= rows; j++){
			}					//		System.out.print(num++ + " ");
			System.out.println();			//	}
		}
	}
}
