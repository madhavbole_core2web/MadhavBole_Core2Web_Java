import java.io.*;

class Pattern9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int rows = Integer.parseInt(br.readLine());

		for(int i = 1; i <= rows; i++){
			char ch = (char)(64+rows);
			for(int j = i; j <= rows; j++){
				System.out.print(ch-- + " ");
			}
			System.out.println();
		}
	}
}
