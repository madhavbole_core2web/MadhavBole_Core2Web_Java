import java.io.*;
class Pattern4{
	public static void main(String[] Madhav)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){
			if(i % 2 != 0){
				char ch = (char)(96+row);
				for(int j = 1; j <= i; j++){
					System.out.print(ch-- + " ");
				}
			}else{
				char ch = (char)(64+row);
				for(int j = 1; j <= i; j++){
					System.out.print(ch-- + " ");
				}
			}
			System.out.println();
		}
	}
}

