import java.io.*;
class Pattern5{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		char ch = (char)(64+row+1);
		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= i; j++){
				System.out.print(ch++ + " ");
			}
			System.out.println();
		}
	}
}

