import java.io.*;
class Pattern6{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		char ch = 'A';
		for(int i = 1; i <= row; i++){
			if(i % 2 != 0){
				for(int j = 1; j <= i; j++){
					System.out.print(j + " ");
					ch++;
				}
			}else{
				for(int j = 1; j <= i; j++){
					System.out.print(ch++ + " ");
				}
			}
			System.out.println();
		}
	}
}

