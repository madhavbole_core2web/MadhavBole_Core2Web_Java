import java.io.*;
class Program3{
        public static void main(String[] Madhav)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the size of array: ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i = 0; i < arr.length; i++){
			System.out.print("Enter " + i + " th element: ");
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Even elements in the array are: ");
		for(int i = 0; i < arr.length; i++){
			if(arr[i] % 2 == 0){
				System.out.println(arr[i]);
			}
		}
		
        }
} 
