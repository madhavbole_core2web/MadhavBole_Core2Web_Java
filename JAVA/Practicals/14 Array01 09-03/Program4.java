import java.io.*;
class Program4{
        public static void main(String[] Madhav)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		int sum = 0;


		for(int i = 0; i < arr.length; i++){
			System.out.print("Enter " + i + " th element: ");
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i = 0; i < arr.length; i++){
			if(arr[i] % 2 != 0){
				sum += arr[i];
			}			
		}

		System.out.println("Sum of odd elements in the array is: " + sum);
        }
} 
