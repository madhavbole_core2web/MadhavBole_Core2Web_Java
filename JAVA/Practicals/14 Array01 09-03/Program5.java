import java.io.*;
class Program5{
        public static void main(String[] Madhav)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];


		for(int i = 0; i < arr.length; i++){
			System.out.print("Enter " + i + " th element: ");
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i = 0; i < arr.length; i++){
			if(arr[i] < 10){
				System.out.println(arr[i] + " is less than 10");
			}	
		}
        }
} 
