import java.io.*;
class Program7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Please enter the size of array: ");
		int size = Integer.parseInt(br.readLine());
		int array[] = new int[size];

		for(int i = 0; i < size; i++){
			array[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Numbers divisible by 4 are: ");
		for(int i = 0; i < size; i++){
			if(array[i] % 4 == 0){
				System.out.println(array[i]);
			}
		}
	}
}

