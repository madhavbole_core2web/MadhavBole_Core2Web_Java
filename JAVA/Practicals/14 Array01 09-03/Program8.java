import java.io.*;
class Program8{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of employee: ");
		int num = Integer.parseInt(br.readLine());
		int age[] = new int[num];

		for(int i = 0; i < age.length; i++){
			age[i] = Integer.parseInt(br.readLine());
		}
	}
}

