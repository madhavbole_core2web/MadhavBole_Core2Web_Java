import java.util.*;
class Program9{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		int number[] = new int[size];

		for(int i = 0; i < size ; i++){
			System.out.print("Enter " + i + " th element: ");
			number[i] = sc.nextInt();
		}

		int k = 1;
		System.out.println("Elements having odd index are: ");
		while(k < size){
			System.out.println(number[k]);
			k += 2;
		}

	}
}
