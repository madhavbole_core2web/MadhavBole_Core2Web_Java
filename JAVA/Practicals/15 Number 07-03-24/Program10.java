import java.util.*;
class Program10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your number: ");
		int num = sc.nextInt();
		int reverse = 0;
		int temp = num;

		while(num != 0){
			int rem = num % 10;
			reverse = reverse * 10 + rem;
			num /= 10;
		}

		if(reverse == temp){
			System.out.println(temp + " is palindrome number");
		}else{
			System.out.println(temp + " is not a palindrome number");
		}
	}
}


