import java.io.*;
class Program4{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter number: ");
                int num = Integer.parseInt(br.readLine());
                int factors = 0;
                int temp = 1;

                while(temp <= num){
                        if(num%temp==0){
                                factors++;
                        }
                        temp++;
                }

                if(factors==2){
                        System.out.println(num + " is not a composite number.");
                }else{
                        System.out.println(num + " is a composite number.");
                }
        }
}

