import java.util.*;
class Program5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number: ");
		int num = sc.nextInt();
		int factorial = 1;
		int temp = num;

		while(temp >= 1){
			factorial = factorial * temp;
			temp--;
		}

		System.out.println("Factorial of " + num + " is: " + factorial);
	}
}

