import java.io.*;
class Program6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number: ");
		int num = Integer.parseInt(br.readLine());
		int factorial = 1;

		for(int i = num; i >= 1; i--){
			factorial = factorial * i;
		}

		System.out.println("Factorial of " + num + " is: " + factorial);
	}
}



