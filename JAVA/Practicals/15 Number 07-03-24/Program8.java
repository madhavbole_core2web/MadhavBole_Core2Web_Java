import java.io.*;
class Program8{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int num = Integer.parseInt(br.readLine());
		int rev = 0;
		int rem;

		while(num != 0){
			rem = num % 10;
			rev = rev * 10 + rem;
			num /= 10;
		}

		System.out.println("Reverse of " + num + " is: " + rev);
	}
}

