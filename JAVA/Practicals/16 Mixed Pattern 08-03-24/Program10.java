import java.util.*;
class Program10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		long num = sc.nextLong();
		long temp = num;
		long rev = 0;

		while(num!=0){
			long rem = num % 10;
			rev = rev*10 + rem;
			num /= 10;
		}

		System.out.println("Reverse of " + temp + " is: " + rev);

		while(rev != 0){
			long rem = rev % 10;
			if(rem % 2 != 0){
				System.out.print(rem*rem + " ");
			}
			rev /= 10;
		}
	}
}

