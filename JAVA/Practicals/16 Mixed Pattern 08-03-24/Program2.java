import java.io.*;
class Program2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());
		char ch = (char)(row+64);

		for(int i = 1; i <= row; i++){
			int num = row + i - 1;
			for(int j = 1; j <= row; j++){
				System.out.print(ch + "" + num + " ");
				num--;
			}
			System.out.println();
		}
	}
}

