import java.util.*;
class Program3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();


		for(int i = 1; i <= row; i++){
			char ch = (char)(row+64);	
			if(i % 2 != 0){
				for(int j = 1; j <= row; j++){
					System.out.print(ch-- + " ");
				}
			}else{
				for(int j = 1; j <= row; j++){
					System.out.print(j + " ");
				}
			}
			System.out.println();
		}
	}
}
