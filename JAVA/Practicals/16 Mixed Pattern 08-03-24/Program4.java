import java.io.*;
class Program4{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){
			int num = row + 1 - i;
			if(i == row || i ==1){
				for(int j = 1; j <= i; j++){
					System.out.print(num++ + " ");
				}
			}else{
				for(int j = 1; j <= i; j++){
					System.out.print(num*j + " ");
				}
			}
			System.out.println();
		}
	}
}
