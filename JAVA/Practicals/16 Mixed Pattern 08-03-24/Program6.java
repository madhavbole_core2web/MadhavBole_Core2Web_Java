import java.io.*;
class Program6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());
				
		for(int i = 1; i <= row; i++){
			char ch = (char)(row+96);
			int num = row;
			if(i % 2 != 0){
				for(int j = 1; j <= i; j++){
					System.out.print(ch-- + " ");
				}
			}else{
				for(int j = 1; j <= i; j++){
					System.out.print(num-- + " ");
				}
			}
			System.out.println();
		}

	}
}
