import java.io.*;
class Program8{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());
		int sum = 0;
		for(int i = 1; i <= row; i++){
			sum += i;
		}

		char ch = (char)(sum+64);

		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row+1-i; j++){
				System.out.print(ch-- + " ");
			}
			System.out.println();
		}
	}
}
