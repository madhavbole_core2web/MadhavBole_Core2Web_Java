import java.io.*;
class Program9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){
			char ch = (char)(64+row+1-i);
			if(i % 2 != 0){
				for(int j = 1; j <= row+1-i; j++){
					System.out.print(j + " ");
				}
			}else{
				for(int j = 1; j <= row+1-i; j++){
					System.out.print(ch-- + " ");
				}
			}
			System.out.println();
		}
	}
}
