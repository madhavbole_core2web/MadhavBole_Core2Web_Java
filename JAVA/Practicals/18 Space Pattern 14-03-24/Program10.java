import java.io.*;
class Program10{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){
			int num = 64+i;
			for(int sp = 1; sp < i; sp++){
				System.out.print(" ");
			}

			for(int j = 1; j <= row-i+1; j++){
				if((row+j-i) % 2 == 0){
					System.out.print(num + "");
				}else{
					System.out.print((char)(num) + "");
				}
				num++;
			}

			System.out.println();
		}
	}
}

