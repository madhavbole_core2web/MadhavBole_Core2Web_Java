import java.io.*;
class Program7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){

			for(int sp = 1; sp < i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= row-i+1; j++){
				System.out.print(j + " ");
			}

			System.out.println();
		}
	}
}
