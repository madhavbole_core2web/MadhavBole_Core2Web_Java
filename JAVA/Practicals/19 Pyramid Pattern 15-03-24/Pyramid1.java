import java.io.*;
class Pyramid1{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){

			for(int sp = 1; sp <= row-i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= 2*i-1; j++){
				System.out.print("1 ");
			}

			System.out.println();
		}
	}
}
