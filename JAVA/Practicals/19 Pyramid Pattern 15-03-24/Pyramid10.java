import java.util.*;
class Pyramid10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		for(int i = 1; i <= row; i++){
			char ch = (char)(64+row+1-i);

			for(int sp = 1; sp <= row-i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= 2*i-1; j++){
				if(j < i){
					System.out.print(ch++ + " ");
				}else{
					System.out.print(ch-- + " ");
				}
			}

			System.out.println();
		}
	}
}
