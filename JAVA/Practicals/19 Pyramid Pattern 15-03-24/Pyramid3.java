import java.io.*;
class Pyramid3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());
		
		int n = row;
		for(int i = 1; i <= row; i++){
			for(int sp = 1; sp <= row-i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= 2*i-1; j++){
				System.out.print(n + " ");
			}

			n--;
			System.out.println();
		}
	}
}
