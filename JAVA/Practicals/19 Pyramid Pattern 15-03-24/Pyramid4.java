import java.util.*;
class Pyramid4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		char ch = 'A';
		for(int i = 1; i <= row; i++){
			for(int sp = 1; sp <= row-i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= 2*i-1; j++){
				System.out.print(ch + " ");
			}

			ch++;
			System.out.println();
		}
	}
}
