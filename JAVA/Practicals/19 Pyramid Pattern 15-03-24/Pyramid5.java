import java.io.*;
class Pyramid5{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){

			int num = 1;

			for(int sp = 1; sp <= row-i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= 2*i-1; j++){
				if(j<i){
					System.out.print(num++ + " ");
				}else{
					System.out.print(num-- + " ");
				}
			}

			System.out.println();
		}
	}
}

