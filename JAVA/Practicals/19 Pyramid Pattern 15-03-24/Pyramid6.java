import java.util.*;
class Pyramid6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		for(int i = 1; i <= row; i++){
			int n = row;

			for(int sp = 1; sp <= row-i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= 2*i-1; j++){
				if(j < i){
					System.out.print(n-- + " ");
				}else{
					System.out.print(n++ + " ");
				}
			}

			System.out.println();
		}
	}
}
