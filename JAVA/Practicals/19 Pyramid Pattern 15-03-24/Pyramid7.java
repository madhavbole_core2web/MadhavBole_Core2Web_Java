import java.io.*;
class Pyramid7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n = 1;
		for(int i = 1; i <= row; i++){
			for(int sp = 1; sp <= row-i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= 2*i-1; j++){
				if(i % 2 == 0){
					System.out.print((char)(64+i) + " ");
				}else{
					System.out.print(i + " ");
				}
			}

			System.out.println();
		}
	}
}
