import java.io.*;
class Pyramid9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){
			int num = 1;

			for(int sp = 1; sp <= row-i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= 2*i-1; j++){

				if(i % 2 == 1){
					System.out.print((char)(64+num) + " ");
					if(j<i){
						num++;
					}else{
						num--;
					}
				}else{
					System.out.print((char)(96+num) + " ");
					if(j<i){
						num++;
					}else{
						num--;
					}
				}
			}
			System.out.println();
		}
	}
}

