import java.util.*;
class Program1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements of array: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = sc.nextInt();
		}

		int count = 0;

		System.out.print("Even numbers in array are: ");
		for(int i = 0; i < arr.length; i++){
			if(arr[i] % 2 == 0){
				System.out.print(arr[i] + " ");
				count++;
			}
		}

		System.out.println("Total count of even numbers in this array is: " + count);
	}
}

