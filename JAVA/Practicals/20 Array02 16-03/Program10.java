import java.io.*;
class Program10{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of array: ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements of array: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int max = arr[0];
		int var = 0;

		for(int i = 1; i < arr.length; i++){
			if(max < arr[i]){
				max = arr[i];
				var = i;
			}
		}

		System.out.println("Maximum number in the array is: " + max + " found at index " + var);
	}
}

