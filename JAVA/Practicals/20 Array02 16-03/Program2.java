import java.io.*;
class Program2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter the size of array: ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int sum = 0;

		System.out.print("Elements divisible by 3 are: ");
		for(int i = 0; i < arr.length; i++){
			if(arr[i] % 3 == 0){
				System.out.print(arr[i] + " ");
				sum += arr[i];
			}
		}
		
		System.out.println();
		System.out.print("Sum of elements divisible by 3 is: " + sum);
		System.out.println();
	}
}



