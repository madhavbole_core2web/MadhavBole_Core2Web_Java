import java.util.*;
class Program4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = sc.nextInt();
		}

		
		System.out.print("Enter the element, you are searching for: ");
		int num = sc.nextInt();

		for(int i = 0; i < arr.length; i++){
			if(arr[i] == num){
				System.out.println(num + " found at index " + i);
				break;
			}
		}
	}
}

