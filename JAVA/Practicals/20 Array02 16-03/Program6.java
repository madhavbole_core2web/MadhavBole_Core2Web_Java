import java.util.*;
class Program6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter the elements in an array: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = sc.nextInt();
		}

		int pro = 1;

		for(int i = 1; i < arr.length; i=i+2){
			pro *= arr[i];
		}

		System.out.println("product of odd indexed elements is: " + pro);
	}
}

