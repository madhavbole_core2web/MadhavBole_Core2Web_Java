import java.io.*;
class Program7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter the size of array: ");
		int size  = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements of array: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		if(size % 2 == 0){
			for(int i = 0; i < arr.length; i += 2){
				System.out.println(arr[i]);
			}
		}else{
			for(int i = 0; i < arr.length; i++){
				System.out.println(arr[i]);
			}
		}
	}
}

