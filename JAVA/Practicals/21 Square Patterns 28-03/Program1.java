import java.util.*;
class Program1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		for(int i = 1; i <= row; i++){
			char ch = (char)(row+64);
			for(int j = 1; j <= row; j++){
				if(i % 2 == 0){
					System.out.print(row + " ");
				}else{
					System.out.print(ch-- + " ");
				}
			}
			System.out.println();
		}
	}
}
