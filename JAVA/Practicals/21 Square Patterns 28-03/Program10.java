import java.util.*;
class Program10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();;
	
		for(int i = 1; i <= row; i++){
			int n = row;
			for(int j = 1; j <= row; j++){
				if(i % 2 == 1 && j % 2 == 1){
					System.out.print(n + " ");
				}else{
					System.out.print((char)(64+n) + " ");
				}
				n--;
			}
			System.out.println();
		}
	}
}
