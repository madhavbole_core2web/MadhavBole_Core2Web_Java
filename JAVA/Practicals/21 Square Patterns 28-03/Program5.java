import java.io.*;
class Program5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n =  row;

		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row; j++){
				if( (i+j-row) % 2 == 0 ){
					System.out.print(n*n + " ");
				}else{
					System.out.print(n + " ");
				}
				n++;
			}
			System.out.println();
		}
	}
}


