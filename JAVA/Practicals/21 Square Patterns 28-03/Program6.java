import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

class Program6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());
		int num = row;

		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row; j++){
				if((row%2==1 && (i+j)%2==0) || (row%2==0 && j%2==0)){
					System.out.print(num * num + " ");
				}else{
					System.out.print(num + " ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
