import java.util.*;
class Program8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();;
	
		for(int i = 1; i <= row; i++){
			char ch = (char)(64+row);
			for(int j = 1; j <= row; j++){
				if((row%2==1 && (i+j)%2==1) || (row%2==0 && (i+j)%2==1)){
					System.out.print(ch-- + " ");
				}else{
					System.out.print("# ");
				}
			}
			System.out.println();
		}
	}
}
