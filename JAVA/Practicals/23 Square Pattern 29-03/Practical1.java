import java.io.*;
class Practical1{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		char ch = (char)(96+row);
		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row; j++){
				if(j == 1){
					System.out.print((char)(ch-32) + " ");
				}else{
					System.out.print(ch + " ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}
