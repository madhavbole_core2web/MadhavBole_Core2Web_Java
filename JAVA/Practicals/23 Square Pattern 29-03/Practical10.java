import java.util.*;
class Practical10{
	public static void main(String[] ags){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		for(int i = 1; i <= row; i++){
			int num = row * (row+1-i);
			int diff = num - 2;

			for(int j = 1; j <= row; j++){
	//			int diff = num - 2;
				if(i == j){
					System.out.print("$ ");
				}else{
					System.out.print(num + " ");
				}
				num += diff;
				diff -= 2;
			}
			System.out.println();
		}
	}
}


