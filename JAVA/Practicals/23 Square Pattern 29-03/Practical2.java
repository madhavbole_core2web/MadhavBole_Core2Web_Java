import java.util.*;
class Practical2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		char ch = (char)(96+row);

		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row-i; j++){
				System.out.print(ch + " ");
				ch++;
			}

			for(int j = 1; j <= i; j++){
				System.out.print((char)(ch-32) + " ");
				ch++;
			}

			System.out.println();
		}
	}
}

