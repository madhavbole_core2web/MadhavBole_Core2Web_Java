import java.io.*;
class Practical5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n =  row;
		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row; j++){
				if(i % 2 == 1 && j % 2 == 0){
					System.out.print("$ ");
					n++;
				}else{
					System.out.print(n + " ");
				}
			}
			System.out.println();
		}
	}
	
}
