import java.io.*;
class Practical7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n = row; 
		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row; j++){
				if(i % 2 == 1 && j % 2 == 1){
					System.out.print((n*n-1) + " ");
				}else{
					System.out.print((n*n) + " ");
				}
				n++;
			}
			System.out.println();
		}
	}
}
