import java.util.*;
class Practical8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();
		int n = row;
		char ch = (char)(97+row);

		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row; j++){
				if(i % 2 == 1 && j % 2 == 1){
					System.out.print((n*n-1) + " ");
				}else{
					System.out.print(ch + " ");
				}
				n++;
				ch++;
			}
			System.out.println();
		}
	}
}
