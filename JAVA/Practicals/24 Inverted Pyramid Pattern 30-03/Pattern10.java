import java.util.*;
class Pattern10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		for(int i = 1; i <= row; i++){
			int n = row+1-i;

			for(int sp = 1; sp < i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= (2*(row-i)+1); j++){
				System.out.print(n + " ");

				if(j <= row-i){
					n--;
				}else{
					n++;
				}
			}
			System.out.println();
		}
	}
}
