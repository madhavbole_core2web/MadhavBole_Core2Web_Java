import java.io.*;
class Pattern5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		char ch = 'A';
		for(int i = 1; i <= row; i++){

			for(int sp = 1; sp < i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= (2*(row-i)+1); j++){
				System.out.print(ch + " ");
			}

			ch++;
			System.out.println();
		}
	}
}
