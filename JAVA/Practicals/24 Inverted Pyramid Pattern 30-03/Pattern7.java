import java.io.*;
class Pattern7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){
			char ch = 'A';

			for(int sp = 1; sp < i; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= (2*(row-i)+1); j++){
				System.out.print(ch + " ");
				if(j <= row-i){
					ch++;
				}else{
					ch--;
				}
			}

			System.out.println();
		}
	}
}
