import java.util.Scanner;
class Array2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int arr[] = {1, 5, 9, 8, 7, 6,};

		System.out.print("Enter the number, you want to search: ");
		int num = sc.nextInt();

		int flag = 0;
		for(int i = 0; i < arr.length; i++){
			if(arr[i] == num){
				flag = 1;
				System.out.println("num: " + num + " first occurred at index: " + i);
				break;
			}
		}

		if(flag == 0){
			System.out.println("num: " + num + " not found in the array.");
		}
	}
}

