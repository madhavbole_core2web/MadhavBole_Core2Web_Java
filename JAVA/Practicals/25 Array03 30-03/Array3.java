import java.io.*;
class Array3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int[] arr = {2, 5, 2, 7, 8, 9, 2};

		System.out.print("Enter specific number: ");
		int num = Integer.parseInt(br.readLine());

		int count = 0;
		for(int i = 0; i < arr.length; i++){
			if(arr[i] == num){
				count++;
			}
		}

		if(count == 0){
			System.out.println("Element not found in array.");
		}else{
			System.out.println("Number: " + num + " occurred " + count + " times in an array");
		}
	}
}

