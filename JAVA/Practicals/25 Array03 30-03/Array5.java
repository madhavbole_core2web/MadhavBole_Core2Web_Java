class Array5{
	public static void main(String[] args){
		int arr[] = {-2, 5, -6, 7, -3, 8};

		for(int i = 0; i < arr.length; i++){
			if(arr[i] < 0){
				arr[i] *= arr[i];
			}	
		}

		for(int j = 0; j < arr.length; j++){
			System.out.print(arr[j] + " ");
		}

		System.out.println();
	}
}
