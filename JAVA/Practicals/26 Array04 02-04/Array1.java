import java.io.*;
class Array1{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of array: ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int total = 0;
		for(int i = 0; i < size; i++){
			total += arr[i];
		}

		int avg = total / size;

		System.out.println("Array element's average is: " + avg);

	}
}
