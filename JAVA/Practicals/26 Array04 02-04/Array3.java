import java.io.*;
class Array3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of array: ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int max = arr[0];
		for(int i = 1; i < size; i++){
			if(arr[i] > max){
				max = arr[i];
			}
		}

		int max2 = arr[0];
		for(int i = 1; i < size; i++){
			if(arr[i] > max2 && arr[i] != max){
				max2 = arr[i];
			}
		}
		
		System.out.println("The second largest element in the array is: " + max2);
	}
}
