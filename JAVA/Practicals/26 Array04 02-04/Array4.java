import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < size; i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Enter the number to check: ");
		int num = sc.nextInt();

		int count = 0;
		for(int i = 0; i < size; i++){
			if(arr[i] == num){
				count++;
			}
		}

		if(count == 2){
			System.out.println(num + " occurs 2 times in the array.");
		}else if(count > 2){
			System.out.println(num + " occurs more than 2 times in the array.");
		}else{
			System.out.println(num + " occurs less than 2 times in the array.");
		}

	}
}
