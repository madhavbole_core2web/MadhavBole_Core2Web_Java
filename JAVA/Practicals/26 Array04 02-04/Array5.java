import java.io.*;
class Array5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of array: ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int temp;
		for(int i = 0; i < size/2; i++){
			temp = arr[i];
			arr[i] = arr[size-1-i];
			arr[size-1-i] = temp;
		}

		for(int i = 0; i < size; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
