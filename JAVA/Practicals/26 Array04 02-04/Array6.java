import java.util.*;
class Array6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < size; i++){
			arr[i] = sc.next().charAt(0);
		}

		int cov = 0;
		int coc = 0;

		for(int i = 0; i < size; i++){
			if(arr[i] == 'a' || arr[i] == 'e' || arr[i] == 'i' || arr[i] == 'o' || arr[i] == 'u' || arr[i] == 'A' || arr[i] == 'E' || arr[i] == 'I' || arr[i] == 'O' || arr[i] == 'U'){
				cov++;
			}else{
				coc++;
			}
		}

		System.out.println("Count of Vowels: " + cov);
		System.out.println("Count of Consonants: " + coc);
	}
}
