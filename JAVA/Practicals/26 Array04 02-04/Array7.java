import java.io.*;
class Array7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of array: ");
		int size = Integer.parseInt(br.readLine());

		char arr[] = new char[size];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < size; i++){
			arr[i] = (char)(br.read());
			br.skip(1);
		}

		for(int i = 0; i < size; i++){
			if(arr[i] >=97 && arr[i] <= 122){
				arr[i] = (char)(arr[i]-32);
			}
		}

		System.out.println("Array after procedure is: ");
		for(int i = 0; i < size; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
