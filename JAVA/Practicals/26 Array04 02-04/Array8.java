import java.util.*;
class Array8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < size; i++){
			arr[i] = sc.next().charAt(0);
		}
		
		System.out.print("Enter the character to check: ");
		char ch = (sc.next().charAt(0));

		int count = 0;
		for(int i = 0; i < size; i++){
			if(arr[i] == ch){
				count++;
			}
		}

		System.out.println(ch + " occurs " + count + " times int the given array.");
	}
}
