import java.io.*;
class Array9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of array: ");
		int size = Integer.parseInt(br.readLine());

		char arr[] = new char[size];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < size; i++){
			arr[i] = (char)(br.read());
			br.skip(1);
		}

		for(int i = 0; i < size; i++){
			if(!(arr[i] >= 'a' && arr[i] <= 'z')){
				System.out.print("# ");
			}else{
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
	}
}
