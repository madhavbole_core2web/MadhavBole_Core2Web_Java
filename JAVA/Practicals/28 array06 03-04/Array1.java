import java.io.*;
class Array1{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of array: ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements of array: ");
		for(int i = 0; i < size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int flag = 0;
		for(int i = 1; i < size; i++){
			if(arr[i-1] < arr[i]){
				flag = 1;
			}
		}

		if(flag == 1){
			System.out.println("Given array is not in descending order.");
		}else{
			System.out.println("Given array is in descending order.");
		}
	}
}
