import java.util.*;
class Array10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter the elements of array: ");
		for(int i = 0; i < size; i++){
			arr[i] = sc.nextInt();
		}
	
		int max1 = arr[0];
		int max2 = arr[0];
		int max3 = arr[0];

		for(int i = 1; i < size; i++){
			if(arr[i] > max1){
				max1 = arr[i];
			}
		}

		for(int i = 1; i < size; i++){
			if(arr[i] > max2 && arr[i] < max1){
				max2 = arr[i];
			}
		}

		for(int i = 1; i < size; i++){
			if(arr[i] > max3 && arr[i] < max2 && arr[i] < max1){
				max3 = arr[i];
			}
		}

		System.out.println("Third largest element is: " + max3);
	}
}
