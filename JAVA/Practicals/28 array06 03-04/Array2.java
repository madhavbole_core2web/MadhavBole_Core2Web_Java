import java.util.*;
class Array2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter the elements of array: ");
		for(int i = 0; i < size; i++){
			arr[i] = sc.nextInt();
		}

		int count = 0;
		int sum = 0;
		for(int i = 0; i < size; i++){
			int factors = 0;
			for(int j = 1; j <= arr[i]/2; j++){
				if(arr[i] % j == 0){
					factors++;
				}
			}

			if(factors == 1){
				count++;
				sum += arr[i];
			}
		}

		System.out.println("Sum of all prime numbers: " + sum); 
		System.out.println("Count of all prime numbers: " + count); 
	}
}
