import java.io.*;
class Array3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of array: ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements of array: ");
		for(int i = 0; i < size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.print("Enter the key: ");
		int key = Integer.parseInt(br.readLine());

		int count = 0;
		for(int i = 0; i < size; i++){
			if(arr[i] == key){
				count++;
				
			}
		}

		if(count > 2){
			for(int i = 0; i < size; i++){
				if(arr[i] == key){
					arr[i] = arr[i] * arr[i] * arr[i];
				}
			}
		}

		if(count == 0){
			System.out.println("Element not found.");
		}

		for(int i = 0; i < size; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
