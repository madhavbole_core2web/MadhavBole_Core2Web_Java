import java.io.*;
class Array5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of first array: ");
		int size1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[size1];

		System.out.print("Enter the size of second array: ");
		int size2 = Integer.parseInt(br.readLine());
		int arr2[] = new int[size2];

		System.out.println("Enter the elements of first array: ");
		for(int i = 0; i < size1; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the elements of second array: ");
		for(int i = 0; i < size2; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		int size3 = size1 + size2;
		int arr3[] = new int[size3];
		
		for(int i = 0; i < size1; i++){
			arr3[i] = arr1[i];
		}
		for(int i = 0; i < size2; i++){
			arr3[size1+i] = arr2[i];
		}

		for(int k = 0; k < size3; k++){
			System.out.print(arr3[k] + " ");
		}
		System.out.println();

	}
}
