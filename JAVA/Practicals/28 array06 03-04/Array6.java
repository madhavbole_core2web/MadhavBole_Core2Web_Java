import java.util.*;
class Array6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter the elements of array: ");
		for(int i = 0; i < size; i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Enter key: ");
		int key = sc.nextInt();

		int flag = 0;
		for(int i = 0; i < size; i++){
			if(arr[i] % key == 0){
				flag = 1;
				System.out.println("An element multiple of " + key + " found at index " + i);
			}
		}

		if(flag == 0){
			System.out.println("Element not found");
		}
	}
}
