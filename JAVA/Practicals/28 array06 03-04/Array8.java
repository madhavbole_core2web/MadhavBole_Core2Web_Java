import java.util.*;
class Array8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the size of array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter the elements of array: ");
		for(int i = 0; i < size; i++){
			arr[i] = sc.next().charAt(0);
		}

		for(int i = 0; i < size/2; i++){
			char temp = arr[i];
			arr[i] = arr[size-1-i];
			arr[size-1-i] = temp;
		}

		System.out.println("Before Reverse: ");
		for(int i = 0; i < size; i+=2){
			System.out.print(arr[size-1-i] + " ");
		}
		System.out.println();
		System.out.println("After Reverse: ");
		for(int i = 0; i < size; i+=2){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
