/*
   Armstrong Number:
   		It is a number where the sum of its own digits, each raised to the power of the number of digits, equals the number itself.

		For ex. num = 153;
		3 * 3 * 3 = 27
		5 * 5 * 5 = 125
		1 * 1 * 1 = 1;
				sum = 27 + 125 + 1 = 153.
		Therefore num is armstrong number.
*/


import java.util.*;
class Number10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number to check: ");
		int num = sc.nextInt();
		int temp = num;
		int digit = 0;
		while(temp != 0){
			digit++;
			temp /= 10;
		}

		int sum = 0;
		int temp2 = num;
		while(temp2 != 0){
			int rem = temp2 % 10;
			int mult = 1;
	
			for(int i = 1; i <= digit; i++){
				mult *= rem;
			}		

			sum = sum + mult;
			temp2 /= 10;
		}

		if(sum == num){
			System.out.println(num + " is an Armstrong number.");
		}else{
			System.out.println(num + " is not an Armstrong number.");
		}
	}
}
