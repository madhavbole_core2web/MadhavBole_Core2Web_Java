/*
 
   A strong number is a number such that the sum of the factorials of all of its digits is equal to the number itself.

   For example, take a number 145.

   The factorial of 1 is: 1! = 1
   The factorial of 4 is: 4! = 24.
   The factorial of 5 is: 5! = 120.

   Adding these factorials together, we get (1 + 24 + 120 = 145) which is equal to the number itself.
   Hence 145 is a strong number.
*/


import java.util.*;
class Number2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of check: ");
		int num = sc.nextInt();

		int temp = num;
		int sum = 0;

		while(temp > 0){
			int fact = 1;
			int rem = temp % 10;
			
			for(int i = rem; i >= 1; i--){
				fact *= i;
			}

			sum = sum + fact;

			temp /= 10;
		}

		if(sum == num){
			System.out.println(num + " is a strong number.");
		}else{
			System.out.println(num + " is not a strong number.");
		}
	}
}
