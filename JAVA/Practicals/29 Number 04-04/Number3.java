/*
 
   If the sum of all divisors of a number is less than that number, then that number is deficient number.

   For ex., num = 8.
   		Divisors of 8 are: 1, 2, 4. (8 is also a divisor of num. But it isn't included.)
		Sum of divisors: 1 + 2 + 4 = 7.
		7 < 8. Therefore 8 is a deficient number.
*/



import java.io.*;
class Number3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number to check: ");
		int num = Integer.parseInt(br.readLine());

		int sum = 0;
		for(int i = 1; i <= num/2; i++){
			if(num / i == 0){
				sum += i;
			}
		}

		if(sum < num){
			System.out.println(num + " is a Deficient number.");
		}else{
			System.out.println(num + " is a not a Deficient number.");
		}
	}
}
