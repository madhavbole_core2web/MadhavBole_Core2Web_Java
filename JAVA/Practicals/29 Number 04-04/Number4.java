/*

   If the sum of all divisors of a number is greater than that number, then that number is Abudant number.

   For ex., num = 12.
                Divisors of 12 are: 1, 2, 3, 4, 6. (12 is also a divisor of num. But it isn't included.)
                Sum of divisors: 1 + 2 + 3 + 4 + 6 = 16.
                16 > 12. Therefore 12 is a Abudant number.
*/


import java.util.*;
class Number4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of check: ");
		int num = sc.nextInt();

		int sum = 0;
		for(int i = 1; i <= num / 2; i++){
			if(num % i == 0){
				sum += i;
			}
		}

		if(sum > num){
			System.out.println(num + " is a Abundant number.");
		}else{
			System.out.println(num + " is not a Abudant number.");
		}
	}
}
