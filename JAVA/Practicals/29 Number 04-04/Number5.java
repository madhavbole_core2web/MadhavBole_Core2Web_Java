/*
 
   Automorphic Number:
   		An automorphic number is a number which is also present at the last of its squre.
		For ex, 25. Square of 25 = 625.
*/

import java.io.*;
class Number5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number to check: ");
		int num = Integer.parseInt(br.readLine());
		int temp = num;
		int sq = num * num;
		int digit = 0;

		while(temp != 0){
			temp /= 10;
			digit++;
		}

		int sqTemp = 0;
		while(digit > 0){
			int rem = sq % 10;
			sqTemp = rem + (sqTemp * 10);
			sq /= 10;
			digit--;
		}
			
		int num2 = 0;
		while(sqTemp > 0){
			int rem = sqTemp % 10;
			num2 = (num2 * 10) + rem;
			sqTemp /= 10;
		}

		if(num2 == num){
			System.out.println(num + " is an Automorphic Number.");
		}else{
			System.out.println(num + " is not an Automorphic Number.");
		}
	}
}
