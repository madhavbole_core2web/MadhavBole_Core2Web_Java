/*
  Fiboncii Series:
  		The Fibonacii series is a sequence of numbers where each number is the sum of the two precceding ones, usually starting with 0 and 1.
*/


import java.util.*;
class Number6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number to print the fibonacci series upto: ");
		int num = sc.nextInt();

		int a = 0;
		int b = 1;

		for(int i = 1; i <= num; i++){
			System.out.print(a + " ");
			int sum = a + b;
			a = b;
			b = sum;
		}
		System.out.println();
	}
}
