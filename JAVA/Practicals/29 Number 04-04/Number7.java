import java.io.*;
class Number7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number: ");
		int num  = Integer.parseInt(br.readLine());

		while(num > 9){
			int sum = 0;
			while(num > 0){
				int r = num % 10;
				sum += (r*r);
				num = num / 10;
			}
			num = sum;
		}

		if(num == 1){
			System.out.println("Happy number");
		}else{
			System.out.println("Not a happy number");
		}
	}
}
