/*
 	Duck Number: 
		Duck Number is a number which contains zero in it.

	For example, 101 : it contains zero.
		     110 : is contains zero.
	     So they are duck numbers.
*/


import java.util.*;
class Number8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of check: ");
		int num = sc.nextInt();
		int temp = num;
		boolean isDuck = false;
		while(num != 0){
			int rem = num % 10;
			if(rem == 0){
				System.out.println("It is a duck number.");
				isDuck = true;
				break;
			}
			num /= 10;
		}

		if(isDuck == false){
			System.out.println(temp + " is not a duck number.");
		}
	}
}
