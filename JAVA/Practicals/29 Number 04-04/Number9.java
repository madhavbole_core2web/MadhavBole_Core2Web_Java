/*

  Harshad / Niven Number:
 			A number is a Harshad number if it is divisible by the sum of its own digits.
		    
	 		For ex. num = 18.

			The sum of its digits is: 1 + 8 = 9.
			18 is divisible by 9.
			Hence 18 is Harshad / Niven number.
*/

import java.io.*;
class Number9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number to check: ");
		int num = Integer.parseInt(br.readLine());
		int temp = num;
		int sum = 0;
		while(num != 0){
			int rem = num % 10;
			sum += rem;
			num /= 10;
		}

		if(temp % sum == 0){
			System.out.println(temp + " is Harshad number.");
		}else{
			System.out.println(temp + " is not a Harshad number.");
		}
	}
}
