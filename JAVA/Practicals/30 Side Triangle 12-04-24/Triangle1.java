import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

class Triangle1{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n = 1;
		for(int i = 1; i < 2*row; i++){
			for(int j = 1; j <= n; j++){
				System.out.print("# ");
			}

			if(i < row){
				n++;
			}else{
				n--;
			}
			System.out.println();
		}
	}
}


