import java.util.*;
class Triangle10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		int n = row;

		for(int i = 1; i <= 2*row-1; i++){

			if(i <= row){
				for(int sp = 1; sp <= row-i; sp++){
					System.out.print("  ");
				}
				n--;
			}else{
				for(int sp = 1; sp <= i-row; sp++){
					System.out.print("  ");
				}
				n++;
			}

			for(int j = n; j < row; j++){
				System.out.print((char)(65+j) + " ");
			}

			System.out.println();
		}
	}
}
