import java.util.*;
class Triangle2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		int n = 1;

		for(int i = 1; i <= 2*row-1; i = i+1){
		
			for(int j = 1; j <= n; j++){
				System.out.print(j + " ");
			}
			
			if(i < row){
				n++;
			}else{
				n--;
			}

			System.out.println();
		}
	}
}
