import java.io.*;

class Triangle3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n = 1;

		for(int i = 1; i <= 2*row-1; i++){
			for(int j = n; j >= 1; j--){
				System.out.print(j + " ");
			}

			if(i < row){
				n++;
			}else{
				n--;
			}

			System.out.println();
		}
	}
}


