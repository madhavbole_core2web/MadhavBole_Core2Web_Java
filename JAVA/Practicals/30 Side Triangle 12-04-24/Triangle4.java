import java.util.*;
class Triangle4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		int n = row;
		int m = 1;

		for(int i = 1; i <= 2*row-1; i++){
			for(int j = 1; j <= m; j++){
				System.out.print(n + " ");
			}

			if(i < row){
				n--;
				m++;
			}else{
				n++;
				m--;
			}

			System.out.println();
		}
	}
}

			
