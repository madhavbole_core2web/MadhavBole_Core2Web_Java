import java.io.*;
class Triangle5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n = row;
		int m = 1;

		for(int i = 1; i <= 2*row-1; i++){
			for(int j = 1; j <= m; j++){
				System.out.print((char)(64+n) + " ");
			}

			if(i < row){
				n--;
				m++;
			}else{
				n++;
				m--;
			}

			System.out.println();
		}
	}
}

			
