import java.util.Scanner;
class Triangle6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows: " );
		int row = sc.nextInt();
		int n = 1;
		for(int i = 1; i <= 2*row-1; i++){
			if(i <= row){
				for(int sp = 1; sp <= row-i; sp++){
					System.out.print("  ");
				}
				n++;
			}else{
				for(int sp = 1; sp <= i-row; sp++){
					System.out.print("  ");
				}
				n--;
			}

			
			for(int j = 1; j < n; j++){
				System.out.print(j + " ");
			}
			
			System.out.println();
		}
	}
}

