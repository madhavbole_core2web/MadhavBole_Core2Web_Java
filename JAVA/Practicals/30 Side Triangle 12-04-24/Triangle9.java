import java.io.*;
class Triangle9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n = row;

		for(int i = 1; i <= 2*row-1; i++){

			if(i <= row){
				for(int sp = 1; sp <= row-i; sp++){
					System.out.print("  ");
				}
				n--;
			}else{
				for(int sp = 1; sp <= i-row; sp++){
				       System.out.print("  ");
				} 
		 		n++;
			}
			
			for(int j = n; j <= row-1; j++){
				System.out.print(j + " ");
			}

			System.out.println();
		}
	}
}
