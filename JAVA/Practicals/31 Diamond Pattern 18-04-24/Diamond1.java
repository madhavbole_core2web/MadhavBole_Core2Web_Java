import java.io.*;
class Diamond1{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());
		int n = 1;

		for(int i = 1; i <= 2*row-1; i++){
			//if( i < row){
				for(int sp = 1; sp <= row-n; sp++){
					System.out.print("  ");
				}
			/*}else{
				for(int sp = 1; sp <= row-n; sp++){
					System.out.print("  ");
				}
			}*/

			for(int j = 1; j <= 2*n-1; j++){
				System.out.print("# ");
			}

			if(i < row){
				n++;
			}else{
				n--;
			}
			
			System.out.println();
		}
	}
}
