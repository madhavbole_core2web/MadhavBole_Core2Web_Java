import java.util.*;
class Diamond2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		int n = 1;
		int c = 1;

		for(int i = 1; i <= 2*row-1; i++){
			for(int sp = 1; sp <= row-n; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= 2*n-1; j++){
				System.out.print(c++ + " ");
			}

			if(i < row){
				n++;
			}else{
				n--;
			}

			System.out.println();
		}
	}
}
