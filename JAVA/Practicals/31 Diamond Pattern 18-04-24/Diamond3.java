import java.io.*;
class Diamond3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n = 1;

		for(int i = 1; i <= 2*row-1; i++){
			for(int sp = 1; sp <= row - n; sp++){
				System.out.print("  ");
			}

			int c = n;

			for(int j = 1; j <= 2*n-1; j++){
				if(j < n){
					System.out.print(c-- + " ");
				}else{
					System.out.print(c++ + " ");
				}
			}

			if(i < row){
				n++;
			}else{
				n--;
			}

			System.out.println();
		}
	}
}
