import java.io.*;
class Diamond9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n = 1;
		char ch = (char)(64+row);

		for(int i = 1; i <= 2*row-1; i++){
			for(int sp = 1; sp <= row-n; sp++){
				System.out.print("  ");
			}

			for(int j = 1; j <= 2*n-1; j++){
				System.out.print(ch + " ");
			}

			if(i < row){
				n++;
				ch--;
			}else{
				n--;
				ch++;
			}

			System.out.println();
		}
	}
}
