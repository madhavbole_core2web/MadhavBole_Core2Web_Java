import java.util.*;
class Program4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		System.out.print("Enter the number of columns: ");
		int col = sc.nextInt();

		int arr[][] = new int[row][col];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				arr[i][j] = sc.nextInt();
			}
		}

		for(int i = 0; i < row; i++){
			int sum = 0;
			if(i % 2 == 0){
				for(int j = 0; j < col; j++){
					sum += arr[i][j];
				}
				System.out.println("Sum of row " + (i+1) + " = " + sum);
			}
		}

	}
}
