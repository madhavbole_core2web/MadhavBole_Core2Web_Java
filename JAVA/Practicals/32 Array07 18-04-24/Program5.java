import java.io.*;
class Program5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		System.out.print("Enter the number of columns: ");
		int col = Integer.parseInt(br.readLine());

		int arr[][] = new int[row][col];

		int sum[] = new int[col];

		for(int i = 0; i < col; i++){
			sum[i] = 0;
		}

		System.out.println("Enter the number of elements in array: ");
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				sum[j] = sum[j] + arr[i][j];
			}
		}

		for(int i = 0; i < sum.length; i++){
			System.out.println("Sum of Column " + (i+1) + " = " + sum[i]);
		}
	}
}

