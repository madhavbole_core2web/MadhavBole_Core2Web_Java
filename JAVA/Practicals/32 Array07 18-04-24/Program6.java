import java.util.*;
class Program6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number of rows: ");
		int row = sc.nextInt();

		System.out.print("Enter the number of columns: ");
		int col = sc.nextInt();

		int arr[][] = new int[row][col];

		System.out.println("Enter the elements in array: ");
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				arr[i][j] = sc.nextInt();
			}
		}

		System.out.println("Elements divisible by 3: ");
		for(int i = 0; i < arr.length; i++){
			for(int j = 0; j < arr[i].length; j++){
				if(arr[i][j] % 3 == 0){
					System.out.println(arr[i][j]);
				}
			}
		}
	}
}
