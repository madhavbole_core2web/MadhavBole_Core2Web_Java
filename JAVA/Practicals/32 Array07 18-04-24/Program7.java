import java.io.*;
class Program7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		System.out.print("Enter the number of columns: ");
		int col = Integer.parseInt(br.readLine());

		int arr[][] = new int[row][col];

		System.out.println("Enter the number of elements in array: ");
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		int product = 1;
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				if(i == j){
					product  *= arr[i][j];
				}
			}
		}
		
		System.out.println("Product of Primary Diagonal : " + product);
	}
}

