import java.io.*;
class Program9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		System.out.print("Enter the number of columns: ");
		int col = Integer.parseInt(br.readLine());

		int arr[][] = new int[row][col];

		System.out.println("Enter the number of elements in array: ");
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		int sum1 = 0;
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				if(i == j){
					sum1 += arr[i][j];
				}
			}
		}

		int sum2 = 0;
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				if(i + j == (row-1)){
					sum2 += arr[i][j];
				}
			}
		}

		System.out.println("Product of Sum of Primary and Secondary Diagonal : " + (sum1*sum2));
	}
}

