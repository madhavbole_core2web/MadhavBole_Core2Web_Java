import java.io.*;
class Program10{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the string: ");
		String str = br.readLine();

		int a = str.length();
		if(a == 0){
			System.out.println("Empty string");
		}else{
			System.out.println(str.charAt(a-1));
		}
	}
}
