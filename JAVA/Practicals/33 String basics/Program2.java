import java.io.*;
class Program2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the first string");
		String str1 = br.readLine();

		System.out.println("Enter the second string: ");
		String str2 = br.readLine();

		String str3 = str1.concat(str2);

		System.out.print("String after concatanation: " + str3);
		System.out.println();
	}
}

