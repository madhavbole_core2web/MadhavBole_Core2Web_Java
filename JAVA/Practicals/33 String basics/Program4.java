import java.io.*;
class Program4{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the first string");
		String str1 = br.readLine();

		System.out.println("Enter the second string: ");
		String str2 = br.readLine();

		System.out.println(str1.compareTo(str2));
	}
}

