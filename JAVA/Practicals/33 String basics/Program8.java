import java.io.*;

class Program8{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the string: ");
		String str = br.readLine();

		str = str.trim();

		System.out.println(str);
	}
}
