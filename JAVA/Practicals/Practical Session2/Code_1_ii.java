class C2W_CharDemo{
	public static void main(String[] args){
		char ch;
		ch = '97';

		System.out.println("char = " + ch);
		System.out.println("Char = " + ch+1);

	}
}

/*
 The datatype "char" takes only a single character as input. But here we have given two characters i.e. 9 & 7.
 Hence, here we will get a compilation error as it is a syntax error.
*/

