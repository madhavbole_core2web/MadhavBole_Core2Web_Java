/*
 Write a program to print the digits from the given number which are divisible by 2
Input : 2569185
Output : digits divisible by 2 are: 8 6 2
*/


class WhileDemo1{
	public static void main(String[] args){
		int num = 2569185;
		while(num != 0){
			int remainder = num % 10;
			if(remainder % 2 == 0){
				System.out.print(remainder + " ");
			}
			num /= 10;
		}
	}
}
