// Write a program to print the sum of odd digits and product of even digits in a given number.

class WhileDemo10{
	static int num = 9367924;
	public static void main(String[] args){
		int sum = 0;
		int pro = 1;

		while(num != 0){
			int rem = num % 10;
			if(rem %2 == 0){
				pro *= rem;
			}else{
				sum += rem;
			}

			num /= 10;

		}
		System.out.println("Sum of Odd digits is: " + sum);
		System.out.println("Product of Even digits is: " + pro);
	}
}


