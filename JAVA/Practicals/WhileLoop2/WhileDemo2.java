/*
 Write a program to print the digits from the given number which is not divisible by 3.
*/

class WhileDemo2{
	public static void main(String[] args){
		int num = 2569185;
		while(num != 0){
			int remainder = num % 10;
			if(remainder % 3 != 0){
				System.out.println(remainder);
			}
			num /= 10;
		}
	}
}
