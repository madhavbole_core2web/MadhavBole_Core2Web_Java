// Write a program to print the digits fromthe given number which divisible by 2 or 3


class WhileDemo3{
	public static void main(String[] args){
		int num = 436980521;
		while(num != 0){
			int remainder = num % 10; 
			if(remainder%2==0 || remainder%3==0){
				System.out.print(remainder);
			}
			num /= 10;
		}

	}
}
