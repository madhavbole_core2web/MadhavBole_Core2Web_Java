// Write a program to print the cubes of even digits of a given number.

class WhileDemo4{
	public static void main(String[] args){
		int num = 256985;
		while(num != 0){
			int remainder = num % 10;

			if(remainder % 2 != 0){
				System.out.print(remainder*remainder + " ");
			}

			num /= 10;
		}
	}
}
