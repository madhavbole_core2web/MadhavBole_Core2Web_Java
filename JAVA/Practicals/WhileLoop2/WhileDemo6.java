// Write a program to print the product of digits in a given number

class WhileDemo6{

	static int num = 234;	
	public static void main(String[] args){
		int product = 1;
		while(num != 0){
			int remainder = num % 10;
			product *= remainder;

			num /= 10;
		}
		System.out.println(product);		
	}
}
