// Write a program to print the sum of even digits


class WhileDemo7{
	public static void main(String[] args){
		int num = 256985;
		int sum = 0;
		while(num != 0){
			int remainder = num % 10;
			if(remainder%2 == 0){
				sum += remainder;
			}

			num /= 10;
		}

		System.out.println(sum);
	}
}
