// Write a program to print the product of odd digits in a given number.
//
class WhileDemo8{
	public static void main(String[] args){
		int num = 256985;
		int pro = 1;

		while(num != 0){
			int rem = num % 10;

			if(rem % 2 != 0){
				pro = pro * rem;
			}

			num /= 10;
		}
		System.out.println(pro);
	}
}

