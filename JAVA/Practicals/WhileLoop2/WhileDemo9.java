// Write a program to calculate the sum of squares of odd digits in the given number.


class WhileDemo9{
	public static void main(String[] args){
		int num = 2469185;
		int sum = 0;

		while(num != 0){
			int rem = num % 10;

			if(rem % 2 != 0){
				sum += (rem*rem);
			}

			num /= 10;
		}
		System.out.println(sum);
	}
}

