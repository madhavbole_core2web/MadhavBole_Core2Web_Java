import java.util.Scanner;

class Question3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		for(int num = sc.nextInt(); num <= 100;){
			System.out.println(num += 10);
		}
	}
}


/*
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		for(int i = num; num <= 100; i++){
			System.out.println(num+=10);
		}
*/
