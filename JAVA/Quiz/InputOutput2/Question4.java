import java.util.*;

class Question4{
	Scanner sc = new Scanner(System.in);
	public static void main(String[] args){
		float num = sc.nextFloat();
		System.out.println(num);
	}
}

/*

class Question4{
	static Sanner sc = new Scanner(System.in);
	public static void main(String[] args){
		float num = sc.nextFloat();
		System.out.println(num);
	}
}

-----------------------------------------------------------------OR---------------------------------------------------------------------------

class Question4{
	Scanner sc = new Scanner(System.in);
	public static void main(String[] args){
		Question4 obj = new Question4();
		float num = obj.sc.nextFloat();
		System.out.println(num);
	}
}

*/


