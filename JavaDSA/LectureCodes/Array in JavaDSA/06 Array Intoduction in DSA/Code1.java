// Print all elements of array.
//
class Code1{
	static void printArray(int arr[], int n){
		for(int i = 0; i < n; i++){
			System.out.print(arr[i] + " ");
			System.out.println();
		}
	}

	public static void main(String[] args){
		int n = 6;
		int arr[] = new int[]{5,6,2,3,1,9};

		printArray(arr, n);
	}
}
