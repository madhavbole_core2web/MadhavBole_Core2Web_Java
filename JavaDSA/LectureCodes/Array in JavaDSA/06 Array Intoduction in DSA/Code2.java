// Given an integer array of size N. Find the count of elements having atleast one element greater than itself.

class Code2{
		static int arr[] = new int[]{2,5,1,4,8,0,8,1,3,8};
		static int n = arr.length;
	public static void main(String[] args){
		int count = 0;
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				if(arr[i] < arr[j]){
					count++;
					break;
				}
			}
		}

		System.out.println(count);
	}
}

class Optimized2{
	public static void main(String[] args){
	Code2 obj = new Code2();

	int count = 0;
	int max = Integer.MIN_VALUE;

	for(int i = 0; i < obj.n; i++){
		if(obj.arr[i] > max){
			max = obj.arr[i];
		}
	}

	for(int i = 0; i < obj.n; i++){
		if(obj.arr[i] == max){
			count++;
		}
	}

	System.out.println(obj.n - count);
}

}
