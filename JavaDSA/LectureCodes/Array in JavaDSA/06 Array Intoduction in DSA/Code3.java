/*
 	Given an array of size N.
	Return the count of pairs(i, j) with Arr[i] + Arr[j] = K.

	Arr : [3, 5, 2, 1, -3, 7, 8, 15, 6, 13]
	N   : 10
	K   : 10 
	Note: i != j

*/

class Code3{
	public static void main(String[] args){
		int Arr[] = new int[]{3,5,2,1,-3,7,8,15,6,13};
		int k = 10;
		int N = 10;
		int count = 0;
		int itr = 0;

		for(int i =0; i < N; i++){
		//	int need = k - Arr[i];
			for(int j = 0; j < N; j++){
				itr++;
				if(Arr[j] + Arr[i] == k && i != j){
					count++;
				}
			}
		}

		System.out.println(count);
		System.out.println(itr);
	}
}

class Optimized3{
	public static void main(String[] args){
		int Arr[] = new int[]{3,5,2,1,-3,7,8,15,6,13};
		int k = 10;
		int N = 10;
		int count = 0;
		int itr = 0;

		for(int i =0; i < N; i++){
		//	int need = k - Arr[i];
			for(int j = i+1; j < N; j++){
				itr++;
				if(Arr[j] + Arr[i] == k && i != j){
					count++;
				}
			}
		}

		System.out.println(count * 2);
		System.out.println(itr);
	}
}
