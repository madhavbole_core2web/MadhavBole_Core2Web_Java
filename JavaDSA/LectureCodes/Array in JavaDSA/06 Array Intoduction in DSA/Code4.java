// Reverse the array.

class Code4{
	static int[] reverseArray(int arr[], int n){
		for(int i = 0; i <= n / 2; i++){
			int temp = arr[i];
			arr[i] = arr[n-1-i];
			arr[n-1-i] = temp;
		}

		int itr = 0;
		for(int num :arr){
			System.out.print(num + " ");
			itr++;
		}

		System.out.println(itr);
		return arr;

	}

	public static void main(String[] args){
		int n = 8;
		int arr[] = new int[]{8,4,1,3,9,2,6,7};

		reverseArray(arr, n);
	}
}
