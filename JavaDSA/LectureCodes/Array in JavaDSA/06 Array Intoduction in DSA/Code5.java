// Find the second largest element in the array. arr[] = [8,4,1,3,9,2,6,7]

class Code5{
	public static void main(String[] args){
		int arr[] = new int[]{8,4,1,3,9,2,6,7};
		int n = 8;

		int max1 = Integer.MIN_VALUE;
		int max2 = Integer.MIN_VALUE;

		for(int i = 0; i < n; i++){
			if(arr[i] > max1)
				max1 = arr[i];

		}


		for(int i = 0; i < n; i++){
			if(arr[i] > max2 && arr[i] < max1)
				max2 = arr[i];
		}

		System.out.println("Second largest element: " + max2);
	}
}

class Optimized5{
	public static void main(String[] args){
		int arr[] = new int[]{8,4,1,3,9,2,6,7};
		int n = 8;

		int max1 = Integer.MIN_VALUE;
		int max2 = Integer.MIN_VALUE;

		for(int i = 0; i < n; i++){
			if(arr[i] > max1){
				max2 = max1;
				max1 = arr[i];
			}

			else if(arr[i] > max2 && arr[i] < max1)
				max2 = arr[i];

		}

		System.out.println(max2);
	}
}
