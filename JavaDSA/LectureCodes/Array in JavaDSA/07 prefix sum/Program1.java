import java.util.*;
class Program1{

	static int [] rangeQuery(int arr[], int Q, int n, Scanner sc){

		//Scanner sc = new Scanner(System.in);
		int preArray[] = new int[n];

		preArray[0] = arr[0];
		for(int i = 1; i < n; i++){
			preArray[i] = preArray[i-1] + arr[i];
		}

		for(int i = 1; i <= Q; i++){
			int s = sc.nextInt();
			int e = sc.nextInt();

			if(s == 0){
				System.out.println(preArray[e]);
			}else{
				System.out.println(preArray[e] - preArray[s-1]);
			}
		}

		return arr;

	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the no of queries: ");
		int Q = 3;

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int n = 10;

		rangeQuery(arr, Q, n, sc);
	}
}
