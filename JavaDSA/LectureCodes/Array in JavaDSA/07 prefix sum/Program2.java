import java.util.*;

class Program2{
	public static void main(String[] args){
		int A[] = new int[]{1,2,3,4,5};
		int n = 5;
		pSum(A, n);
	}

	static int[] pSum(int A[], int n){

		for(int i = 1; i < n; i++){
			A[i] = A[i-1] + A[i];
		}

		
		for(int i = 0; i < n; i++){
			System.out.print(A[i] + " ");
		}

		return A;
	}
}
