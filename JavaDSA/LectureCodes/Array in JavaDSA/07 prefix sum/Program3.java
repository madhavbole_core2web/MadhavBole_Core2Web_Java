/*
    Problem Description:
    	Givan an integer array A of size N and integer B, you have to return the same array after rotating it B times towards rigth.

    Problem Constraints:
    	1 <= N <= 10^5
	1 <= A[i] <= 10^9
	1 <= B >= 10^9

    Example Input:

    	Input 1:
		A = [1,2,3,4]
		B = 2

	Input 2:
		A = [3,4,1,2]
		B = 2

    Example Output:
    	
    	Output 1:
		[3,4,1,2]

	Output:
		[1,2,3,4]

*/

import java.util.*;
class Program3{
	static void rotateArray(int arr[], int n, int B){
		int p = 1;
		while(p <= B){
			int temp = arr[n-1];
			for(int i = 1; i < n; i++){
				arr[n-i] = arr[n-1-i];
			}
			arr[0] = temp;
			p++;
		}

		for(int i = 0; i < n; i++){
			System.out.print(arr[i] + " ");
		}

		System.out.println();
	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the B : ");
		int B = sc.nextInt();

		int [] arr  = new int[]{1,2,3,4};

		int n = 4;

		rotateArray(arr, n, B);
	}
}
