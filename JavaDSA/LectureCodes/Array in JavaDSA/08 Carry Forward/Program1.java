/*
 
   Given an integer array of size N. Build an LeftMax array of size n. LeftMax of i contains the maximum for the index 0 to index i.

	arr 	: [-3,6,2,4,5,2,8,-9,3,1]
	N   	: 10
	LeftMax	: [-3,6,6,6,6,6,8,8,8,8]

*/


class program1{
	static void carryForward(int arr[], int n, int LeftMax[]){
		int max = Integer.MIN_VALUE;

		for(int i = 0; i < n; i++){
			for(int j = 0; j <= i; j++){
				if(arr[j] > max){
					max = arr[j];
				}
			}

			LeftMax[i] = max;
		}

		for(int i = 0; i < n; i++){
			System.out.print(LeftMax[i] + " ");
		}
	}
	public static void main(String[] args){
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int n = 10;
		int LeftMax[] = new int[n];

		carryForward(arr, n, LeftMax);
	}
}

class Optimized1{
	public static void main(String[] args){
		
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int n = 10;
		int LeftMax[] = new int[n];

		int max = Integer.MIN_VALUE;

		LeftMax[0] = arr[0];
		for(int i = 1; i < n; i++){
			if(arr[i] > LeftMax[i-1]){
				max = arr[i];
			}
			LeftMax[i] = max;
		}


		for(int i = 0; i < n; i++){
			System.out.print(LeftMax[i] + " ");
		}
	}
}
