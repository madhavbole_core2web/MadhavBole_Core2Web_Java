//	Right max array 

class Program2{
	public static void main(String[] args){
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int n = 10;
		int RightMax[] = new int[n];

		int max = Integer.MIN_VALUE;

		RightMax[n-1] = arr[n-1];
		for(int i = n-2; i >= 0; i--){
			if(arr[i] > RightMax[i+1]){
				max = arr[i];
			}
			RightMax[i] = max;
		}

		for(int i = 0; i < n; i++){
			System.out.print(RightMax[i] + " ");
		}
	}
}
