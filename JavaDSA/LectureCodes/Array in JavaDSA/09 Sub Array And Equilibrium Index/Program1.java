/*

   EQUILIBRIUM INDEX PROBLEM:
---------------------------------
   
   Problem Description:
  	=> You are given an array A of integers of size N.
	=> Your task is to find the equilibrium index of the given array.
	=> The equilibrium index of an array is an index such that the sum of elements at lower indices is equal to the sum of elements at higher indices.
	=> If there are no elements that are at lower indices or at higher indices, then the corresponding sum of elements is considerd as 0.
   
   Note:
	=> Array indexing starts from 0.
     	=> If there is no equilibrium index, then return -1.
	=> If there are more than an equilibrium indexes then return the minimum index.

   Problem Constraint:
	=> 1 <= N <= 10^5
     	=> 10^5 <= A[i] <= 10^5

   Input Example:
	
	Input :
     		Arr : [-7,1,5,2,-4,3,0]
		Arr : [1,2,3]	

	Output:
		3
		-1

*/


class Program1{
	static int EquiIndex(int arr[]){
		for(int i = 0; i < arr.length; i++){
			int leftsum = 0;
			int rightsum = 0;

			for(int j = 0; j < i; j++){
				leftsum += arr[j];
			}

			for(int j = i+1; j < arr.length; j++){
				rightsum += arr[j];
			}

			if(leftsum == rightsum){
				return i;
			}
		}
		return -1;
	}

	public static void main(String[] args){
		int arr[] = new int[]{-7,1,5,2,-4,3,0};
		
		System.out.println(EquiIndex(arr));
	}
}

class Optimized1{
	public static void main(String[] args){
		int arr[] = new int[]{0,3,-4,2,5,1,-7};
		
		System.out.println(EquiIndex(arr));
	}

	static int EquiIndex(int arr[]){
		int n = arr.length;
		int newArr[] = new int[n];

		newArr[0] = arr[0];
		for(int i = 1; i < n; i++){
			newArr[i] = newArr[i-1] + arr[i];
		}

		if(newArr[n-1] - newArr[0] == 0){
			return 0;
		}

		for(int i = 1; i < n; i++){
			if(newArr[i-1] == (newArr[n-1] - newArr[i]))
					return i;
		}

		return -1;
	}
}

