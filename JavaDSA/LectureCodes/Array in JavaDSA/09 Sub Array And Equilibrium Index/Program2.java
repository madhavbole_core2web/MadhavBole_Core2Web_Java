/*
 
   SUB ARRAY PROBLEM:
 ------------------------
 	
 	=> Given an integer array of size N.
	=> Return the length of the smallest subarray which contains both the maximum of the array and the minimum of the array.

		Arr : [1,2,3,1,3,4,6,4,6,3]


*/



class Program2{
	public static void main(String[] args){
		int arr[] = new int[]{1,2,3,1,3,4,4,6,6,3};
		int n = arr.length;

		int itr = 0;
		int maxnum = Integer.MIN_VALUE;
		int minnum = Integer.MAX_VALUE;
		int count = Integer.MAX_VALUE;

		for(int i = 0; i < n; i++){
			if(arr[i] > maxnum)
				maxnum = arr[i];
		
			if(arr[i] < minnum)
				minnum = arr[i];
		}
	

		for(int i = 0; i < n; i++){

			if(arr[i] == minnum){
				for(int j = i+1; j < n; j++){
					if(arr[j] == maxnum){
						count = Math.min(count, (j-i)) + 1;
						break;
					}
					itr++;
				}
			}

			else if(arr[i] == maxnum){
				for(int j = i+1; j < n; j++){
					if(arr[j] == minnum){
						count = Math.min(count, (j-i)) + 1;
						break;
					}
					itr++;
				}
			}

		}

		System.out.println(count);
		System.out.println(itr);
	}
}

