import java.util.*;

class Optimization2{
	static int findFactors(int num){
		int count = 0;
		int itr = 0;

		for(int i = 1; i <= num/2; i++){
			if(num % i == 0){
				count++;
			}
			itr++;
		}
		System.out.println("Number of iterations : " + itr);

		return count+1;
	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number: ");
		int num = sc.nextInt();

		int count = findFactors(num);
		System.out.println(count);
	}
}
