import java.util.*;

class Optimization4{
	static int findFactors(int num){
		int count = 0;
		int itr = 0;

		for(int i = 1; i*i <= num; i++){
			itr++;
			if(num % i == 0){
				if(i == num/i)
					count += 1;
				else
					count += 2;
			}
		}

		System.out.println("Number of iterations: " + itr);
		return count;
	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number: ");
		int num = sc.nextInt();

		int count = findFactors(num);
		System.out.println(count);
	}
}
