import java.util.*;

class Code2{
	static int SquareRoot(int num){
		int val = 0;
		for(int i = 1; i <= num; i++){
			if(i*i >= num && (i-1)*(i-1) <= num){
				val = i-1;
				break;
			}
		}
		return val;
	}


	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number: ");
		int num = sc.nextInt();

		int ret = SquareRoot(num);
		System.out.println(ret);
	}
}

/*

import java.util.*;

class Code1{
	static int SquareRoot(int num){
		int val = 0;
		for(int i = 1; i <= num; i++){
			if(i*i <= num){
				val = i;
				break;
			}
		}
		return val;
	}


	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number: ");
		int num = sc.nextInt();

		int ret = SquareRoot(num);
		System.out.println(ret);
	}
}
*/

