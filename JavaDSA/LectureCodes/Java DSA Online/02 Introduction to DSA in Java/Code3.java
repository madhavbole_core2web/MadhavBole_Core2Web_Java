import java.util.*;

class Code3{

	static int SquareRoot(int num){
		int val = 0;
		int start = 1;
		int end = num;

		while(start <= end){
			int mid = (start + end) / 2;
			int sqr = mid * mid;
			
			/*if(sqr == num){
				return mid;
			}*/

			if(sqr > num){
				end = mid - 1;
			}else{
				start = mid + 1;
				val = mid;
			}
		}
		return val;
	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number: ");
		int num = sc.nextInt();

		int ret = SquareRoot(num);
		System.out.println(ret);
	}
}
		
