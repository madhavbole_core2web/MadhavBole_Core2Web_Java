import java.util.*;

class Code4{

	static int squareRoot(int num){
		int start = 1;
		int end = num;
		int val = 0;

		while(start <= end){
			int mid = (start + end) / 2;
			
			if(mid*mid > num){
				end = mid - 1;
			}
			else if(mid*mid < num){
				start = mid + 1;
				val = mid;
			}
			else{
				return mid;
			}
		}
		return val;

	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number: ");
		int num = sc.nextInt();

		int ans = squareRoot(num);
		System.out.println(ans);
	}
}
