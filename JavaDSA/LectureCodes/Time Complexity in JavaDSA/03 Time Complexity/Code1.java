import java.util.*;
class Code1{

	static long numberSum(int num){
		long numberSum = 0;

		for(int i = 1; i <= num; i++){
			numberSum += i;
		}
		
		return numberSum;
	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number : ");
		int num = sc.nextInt();

		long ans = numberSum(num);
		System.out.println("Sum = " + ans);
	}
}
