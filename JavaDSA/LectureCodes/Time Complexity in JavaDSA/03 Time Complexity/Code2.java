import java.util.*;
class Code2{
	static int numberSum(int num){
		int add = (num * (num+1)) / 2;
		
		return add;
	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number : ");
		int num = sc.nextInt();

		int ans = numberSum(num);
		System.out.println("Sum = " + ans);
	}
}
